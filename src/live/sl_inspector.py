# -*- coding: utf-8 -*-
import streamlit as st
from streamlit.logger import get_logger
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import os, sys
import json
import numpy as np
from numpy.core.fromnumeric import size
import pandas as pd
from tqdm import tqdm

import random
import cv2
import pickle
import time
import copy
import glob

from mmcv import Config

#from src.features.skeleton_pipelines import NanToZeros
import matplotlib.pyplot as plt
import seaborn as sns
#from src.features.skeleton_pipelines import NormalizeSkeleton
from mmaction.datasets.pipelines import Compose

# from src.features.skeleton_pipelines import NanToZeros
# from src.features.skeleton_pipelines import GetMiddleFrame
# from src.features.skeleton_pipelines import SelectCondensedHandsAndArms
# from src.features.skeleton_pipelines import NormalizeSkeleton
# from src.features.skeleton_pipelines import FormatKPAndScore

from src.models.utils import generate_model_evaluation_summary
from src.models.utils import find_mislabeled
from src.features.utils import CONDENSED_KPTS
from src.features.utils import CONDENSED_KPTS_LABELS

LOGGER = get_logger(__name__)

@st.cache(suppress_st_warning=True)
def load_model_outputs(anno_path):
    """Returns annotation file(pkl) with saved model outputs"""
    with open(anno_path, 'rb') as handle:
        anno = pickle.load(handle)
    return anno

@st.cache(suppress_st_warning=True)
def load_skeleton_data_anno(data_id, cv_id, class_num):
    """Returns annotation file(pkl) with saved skeletal data"""
    anno_path = os.path.join(project_dir, 'data/processed', data_id, 
        'annotations_'+str(class_num), cv_id+'_skeleton.pkl') 
    with open(anno_path, 'rb') as handle:
        anno = pickle.load(handle)
    return anno

@st.cache
def get_df_from_anno(anno):
    """Returns dataframe with video ids and labels"""
    df = pd.DataFrame(columns=['frame_dir', 'label'])
    frame_dir = []
    label = []
    for i, s in enumerate(anno):
        frame_dir.append(s['frame_dir'])
        label.append(s['label'])
    df['frame_dir'] = frame_dir
    df['label'] = label
    return df

@st.cache
def get_pose_video_as_array(frame_dir, dataset_id, pose_config='wbcoco_mmpose'):
    """Returns video as array"""
    
    if dataset_id == 'IsoGD':
        vid_path = os.path.join(project_dir, 'data/processed',
            os.path.splitext(frame_dir)[0], 'vis_' + pose_config + '_config.mp4'
        )
    elif dataset_id == 'SLR':
        vid_path = os.path.join(project_dir,
            os.path.splitext(frame_dir)[0], 'vis_' + pose_config + '_config.mp4'
        )
    frames = []
    cap = cv2.VideoCapture(vid_path)
    ret = True
    while ret:
        ret, img = cap.read() # read one frame from the 'capture' object; img is (H, W, C)
        if ret:
            # img = img[:, :, ::-1] # BGR->RGB
            frames.append(img)
    video = np.stack(frames, axis=0) # dimensions (T, H, W, C)
    return video

@st.cache
def run_data_pipeline(anno_sample):
    clip_len = anno_sample['total_frames']
    reshape_img_size = 128
    sel_bones = (
                (5, 6), (5, 7),
                (6, 8), (8, 10), (7, 9), (9, 11), 
                (12,13),(12,14),(12,16),(12,18),(12,20),
                (14,15),(16,17),(18,19),(20,21),
                (22,23),(22,24),(22,26),(22,28),(22,30),
                (24,25),(26,27),(28,29),(30,31),
                (10,12),(11,22)
            )
    left_kp = [1,3,5] + [7,8,9,10,11,12,13,14,15,16]
    right_kp = [2,4,5] + [17,18,19,20,21,22,23,24,25,26]
    skeletons = [(bone[0]-5, bone[1]-5) for bone in sel_bones]
    frame_interval = 1
    num_clips = 1
    test_pipeline = [
        dict(
            type='SampleFrames', 
            clip_len=clip_len, 
            frame_interval=frame_interval, 
            num_clips=num_clips
        ),
        dict(type='PoseDecode'),
        dict(type='PoseCompact', hw_ratio=1., allow_imgpad=True),
        dict(type='Resize', scale=(-1, reshape_img_size)),
        dict(type='CenterCrop', crop_size=reshape_img_size),
        dict(
            type='GeneratePoseTarget',
            sigma=0.6,
            use_score=True,
            with_kp=False,
            with_limb=True,
            skeletons=skeletons,
            left_kp=left_kp,
            right_kp=right_kp,
            ),
    ]
    
    pipeline = Compose(test_pipeline)
    anno_sample['start_index'] = 0
    anno_sample['modality'] = 'PoseDataset'
    anno_sample['keypoint'] = anno_sample['keypoint'][:,:,CONDENSED_KPTS]
    anno_sample['keypoint_score'] = anno_sample['keypoint_score'][:,:,CONDENSED_KPTS]
    # thr = 0.0s
    # mask = anno_sample['keypoint_score'].squeeze() < thr
    # anno_sample['keypoint'][0][mask] = 0
    # anno_sample['keypoint_score'][0][mask] = 0
    #########################################################
    output = pipeline(anno_sample)
    return output #output['imgs'].mean(axis=-1)

@st.cache
def get_skeleton_img(anno_sample):
    arr = anno_sample['imgs']
    skeleton_arr = arr.mean(axis=-1)    
    skeleton_arr /= np.max(skeleton_arr)
    return skeleton_arr


def run():
    # Load .dotenv environment variables needed
    DATA_ROOT = os.environ.get("")

    st.title('Model-Data Inspector')
        # collect data outputs that were run on valid or test sets
    try:
        pkl_files = glob.glob("models/outputs/*.pkl")
        config_ids = [s[s.find('outputs/')+8:s.find('_cv')]+'.py' for s in pkl_files]
        config_ids = list(dict.fromkeys(config_ids))
        config_id = st.selectbox("Choose model config", 
            config_ids, 0
        )
    except Exception as e: st.error(e)
    config_expander = st.expander("Configuration file")
    cfg = Config.fromfile(os.path.join('models/configs/mmaction/', config_id))
    config_expander.text(cfg.text)
    data_id = cfg.data_id
    cv_id = st.selectbox("Choose data CV type", ['test', 'valid'], 0)
    
    # load data output and report summary
    try:
        data_path = os.path.join(project_dir, 
            'models/outputs', config_id[:-3]+'_cv-'+cv_id+'.pkl'
        )
        model_outputs = load_model_outputs(data_path)
        df_scores, df_cm, mean_class_acc, top_1_acc, top_5_acc = \
            generate_model_evaluation_summary(model_outputs)
    except Exception as e:
        st.error(e)
    
    perf_expander = st.expander("Model Performance")
    perf_expander.text(f'Mean Class Accuracy: {mean_class_acc:.04f}')
    perf_expander.text(f'Top 1 Accuracy: {top_1_acc:.04f}')
    perf_expander.text(f'Top 5 Accuracy: {top_5_acc:.04f}')
    perf_expander.text('Samples: ' + str(df_scores['Support'].sum()))
    perf_expander.dataframe(df_scores)
    cm_fig = sns.heatmap(
        df_cm, annot=False, cmap='viridis').set_title('Confusion Matrix').get_figure()
    perf_expander.pyplot(cm_fig)
    #perf_expander.bar_chart(df_scores[['Accuracy', 'Support']])
    # fig = sns.barplot(x='Label', y='Accuracy', data=df_scores).get_figure()
    # perf_expander.pyplot(fig)
    
    # Inspect data
    sample_idx = 5
    sample_idx_widget = st.sidebar.empty()
    sample_idx_input = sample_idx_widget.number_input(
        'Sample Index', min_value=0, 
        max_value=int(df_cm.sum().sum()), value=sample_idx, step=1
    )
    sample_idx = int(sample_idx_input)
    # load pose if of pose type
    if cfg.dataset_type == 'PoseDataset':
        skeleton_anno = load_skeleton_data_anno(data_id, cv_id, cfg.num_classes)
        # sample_idx_input = st.sidebar.number_input(
        #     'Sample Index', min_value=0, 
        #     max_value=len(skeleton_anno), value=5, step=1
        # )
        # sample_idx = int(sample_idx_input)
        # mi = st.empty()
        skeleton_sample = copy.deepcopy(skeleton_anno[sample_idx])
        pose_video_arr = get_pose_video_as_array(skeleton_sample['frame_dir'],
            data_id
        )
        pipeline_output = run_data_pipeline(skeleton_sample)
        skeleton_arr = get_skeleton_img(pipeline_output)

        # select frame to plot
        frame_idx = st.sidebar.slider("Frame", 1, skeleton_arr.shape[0], 0, 1)
        col1, col2 = st.columns(2)
        pose_img = col1.empty()
        skeleton_plot = col2.empty()
        pose_img.image(
            pose_video_arr[frame_idx], use_column_width=True,
            channels='BGR', caption=None
        )
        skeleton_plot.image(
            skeleton_arr[frame_idx], use_column_width=True,
            caption='Condensed hand and arm skeleton'
        )
        # plot keypoint confidence
        kpt_score_data = pd.DataFrame(
            pipeline_output['keypoint_score'].squeeze()[frame_idx],
            index=CONDENSED_KPTS_LABELS, columns=['frame-'+str(frame_idx)]
        )

        keypoint_expander = st.expander("Keypoint Confidence")
        keypoint_expander.bar_chart(kpt_score_data)
        
        

    # Display Scores
    output_expander = st.expander("Example output")
    y_hat = model_outputs['y_hat'][sample_idx]
    y = model_outputs['labels'][sample_idx]
    df_model_score_output = pd.DataFrame(
        {'Model Score' : y_hat, 'Label' : df_scores['Label']}
    )
    output_expander.text('Label: ' + str(y))
    pred = np.argmax(y_hat)
    output_expander.text('Prediction: ' + str(pred))
    output_expander.dataframe(df_model_score_output)


    # display information about misclassifications
    mislabel_filter = st.sidebar.multiselect(
        'Mislabeled predictions to include:',
             df_scores['Label']
    )
    find_mislabeled_button = st.sidebar.button('Find mislabeled example')
    if find_mislabeled_button:
        mislabeled_indices = find_mislabeled(model_outputs, mislabel_filter)
        # randomly select index
        sample_idx = random.choice(mislabeled_indices)
        sample_idx_input = sample_idx_widget.number_input(
            'Sample Index', min_value=0, 
            max_value=int(df_cm.sum().sum()), value=sample_idx, step=1
        )
        sample_idx = int(sample_idx_input)
        # skeleton_sample = copy.deepcopy(skeleton_anno[sample_idx])
        # print(sample_idx)


    
#         # layout 
#         widget_top_1 = st.empty()
#         widget_top_2 = st.empty()
#         col1, col2 = st.beta_columns(2)
#         pose_img = col1.empty()
#         skeleton_plot = col2.empty()
#         widget_top_3 = st.empty()
#         if st.sidebar.button('Inspect'):
#             widget_top_1.progress(0)
#             for frame_idx in range(pose_video_arr.shape[0]):
#                 progress = int(100*frame_idx/sample['total_frames'])
#                 widget_top_1.progress(progress+1)
#                 widget_top_2.text("Frame %i/%i" % ((frame_idx+1), sample['total_frames']))
#                 time.sleep(1/sample['fps'])
#                 caption = "Pose video output -- BBox Score: %f" \
#                     % (np.squeeze(sample['bbox_score'])[frame_idx])
#                 pose_img.image(pose_video_arr[frame_idx], use_column_width=True,
#                     channels='BGR', caption=caption
#                 )
#                 skeleton_plot.image(skeleton_arr[frame_idx], use_column_width=True,
#                     caption='Condensed hand and arm skeleton')

#         # slider
        # widget_top_2.empty()
#         frame_idx = widget_top_1.slider("Frame", 1, sample['total_frames'], 0, 1)-1
#         caption = "Pose video output -- BBox Score: %f" \
#                     % (np.squeeze(sample['bbox_score'])[frame_idx])
#         pose_img.image(pose_video_arr[frame_idx], use_column_width=True,
#             channels='BGR', caption=caption
#         )
#         skeleton_plot.image(skeleton_arr[frame_idx], use_column_width=True,
#             caption='Condensed hand and arm skeleton')

#         # plot keypoint confidence
#         kpt_score_data = pd.DataFrame(
#             output['keypoint_score'].squeeze()[frame_idx],
#             index=CONDENSED_KPTS, columns=[frame_idx]
#         )
#         widget_top_3.bar_chart(kpt_score_data)













    # elif inspect_id == 'Performance':

    #     try: 
    #     # load model results
    #     pkl_path = os.path.join(project_dir, 'models/outputs', model_id+'.pkl')
        
    #     val_expander = st.beta_expander("Validation Results")
    #     anno_df = get_df_from_anno(anno)
    #     selected_labels = val_expander.multiselect('Select labels', np.sort(anno_df['label'].unique()))

    #     df_scores = generate_model_evaluation_summary(pkl_path, data_id)
    #     # Average Acc
    #     val_expander.write(df_scores)

        # cm_file_temp = 'data-{data_id}_cv-{cv_id}_model-{model_id}.csv'
        # df_cm = pd.read_csv(
        #     os.path.join(project_dir, 'reports/confusion',
        #         cm_file_temp.format(data_id=data_id, cv_id=cv_id, model_id=model_id)),
        #     index_col=0
        # )
        # st.pyplot(sns.heatmap(df_cm, annot=False, cmap='viridis').get_figure())
        
        # s = 10
        # # worst s # plot worst 10x10 misclassfied
        



        # # 


        # fig = sns.barplot(x='Class ID', y='Percent Correct', data=df_scores).get_figure()
        #df_cm_worst = df_cm[sorted_idx[0:s]].iloc[sorted_idx[0:s]]
        #df_cm_best = df_cm[sorted_idx[-s::]].iloc[sorted_idx[-s::]]
        # plot worst

        # plot best
        
        # sort
        # np.diag(df_cm.to_numpy())

        # from src.visualization.visualize import 
        # st.pyplot()

            # Fetch benchmark results
            # Benchmark
            # validation - test

            # Confusion matrix
            # validation - test


    # widget layout

    

if __name__ == "__main__":
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]
    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    run()
















# if st.button('Animate'):


#img_slider.slider("Frame", 0, pose_video_arr.shape[0], img_idx, 1)
# st.video(video)

# import imageio
# video = imageio.mimread(pose_video_path)
# st.video(video)
# cap = cv2.VideoCapture(pose_video_path)
# while (cap.isOpened()):
#     flag, img = cap.read()
#     if not flag:
#         break
#st.video(cap,start_time=0)

# selected skeleton images
#hour_to_filter = st.slider('hour', 0, 23, 17)  # min: 0h, max: 23h, default: 17h