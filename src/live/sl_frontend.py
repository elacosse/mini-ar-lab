import logging
import logging.handlers
import threading

import os
from pathlib import Path
from typing import List, NamedTuple

import av
import cv2
import matplotlib.pyplot as plt
import numpy as np
import streamlit as st

from streamlit_webrtc import (
    RTCConfiguration,
    VideoProcessorBase,
    WebRtcMode,
    webrtc_streamer,
)

import time 
import random
from collections import deque
import src.features.Pose as Pose
import src.models.Detector as Detector
import src.models.Model as Model
from mmcv import Config, DictAction

logger = logging.getLogger(__name__)

################################################################################
RTC_CONFIGURATION = RTCConfiguration(
    {"iceServers": [{"urls": ["stun:stun.l.google.com:19302"]}]}
)
HERE = Path(__file__).parent
DEVICE_POSE = "cuda:0" # which device to run on?
DETECTOR_POSE_CONFIG_FILE = 'wbcoco_mmpose_config.yml' # config for detector

DRAWING_FPS = 20 # Set upper bound FPS value of the output drawing
INFERENCE_FPS = 20 # Set upper bound FPS value of model inference

# Pose estimation feed with threshold options
DEFAULT_KPT_THRESHOLD = 0.3 # Keypoint Threshold for pose model
DEFAULT_BBOX_THRESHOLD = 0.3 # Bounding box threshold for pose model
# Action classification threshold options
DEFAULT_SCORE_THRESHOLD = 0.5 # Recognition model score threshold
DEFAULT_DETECT_THRESHOLD = 0.0 # Detection model score threshold
DEFAULT_REFRACTORY_FRAME_PERIOD = 12 # After how many frames before next inference
TEXT_OUT_TIME = 3.0 # How long to show action text (seconds)
################################################################################

def main():
    st.header("CV-Streamlit-WebRTC")
    ar_page = 'Real-time action-recogntion.'

    ### Select an action recognition model
    # model_config_list = [
    #     'posec_32x2x1_SLR_data-IsoGD_249.py',
    #     'posec_16x4x1_IsoGD249_data-IsoGD_11.py'
    # ]

    # Automatically collect all the model configs from directory
    for file in os.listdir(os.path.join(project_dir, 'models/configs/mmaction')):
        if file.endswith(".py"):
            model_config_list.append(file)

    model_config_file  = st.selectbox(
        'Select a model configuration file',
        model_config_list,
    )
   
    ### Select a detection model
    detect_model_config_list = [None]
    detect_model_config_file  = st.selectbox(
        'Select a detection model configuration file',
        detect_model_config_list,
    )

    ### Run complete application
    pose_ar_app(model_config_file, detect_model_config_file)

    logger.debug("=== Alive threads ===")
    for thread in threading.enumerate():
        if thread.is_alive():
            logger.debug(f"  {thread.name} ({thread.ident})")



def pose_ar_app(model_config_file: str, detect_model_config_file: str) -> None:
    """ Run the complete application. 

    Args:
        model_config_file (str): path to model config file
        detect_model_config_file (str): path to detection model config file
    Returns:
        None
    """

    ################################################################################
    ### Load selected model configuration ##########################################
    ### Model ######################################################################
    model_config_path = os.path.join(
        project_dir, 'models/configs/mmaction', model_config_file
    )
    model_cfg = Config.fromfile(model_config_path)
    model_checkpoint = os.path.join(model_cfg.work_dir, 'latest.pth')
    ### Detector ###################################################################
    detect_model_config_path = os.path.join(
        project_dir, 'models/configs/mmaction', detect_model_config_file
    )
    detect_model_cfg = Config.fromfile(detect_model_config_path)
    detect_model_checkpoint = os.path.join(detect_model_cfg.work_dir, 'latest.pth')
    ################################################################################
    class ARVideoProcessor(VideoProcessorBase):
        """ Video processor for action recognition for WebRTC Streamer.

        Attributes:
        -----------
        pose : pose object
            MMCV-based model that can process RGB images to skeletons
        render_pose : bool
            Flag whether to render the skeleton over RGB feed 
        detector : detector object  
            model to determine whether to collect frames for action model (queue 1)
        detect_sample_length : int
            size of frame queue for detector model (queue 1)
        model : action recognition object
            model for performing action recognition (queue 2)
        score_thr : float
            threshold for model output to 
        action_sample_length : int
            size of frame queue for recognizer model (queue 2)
        detect_queue : deque
            deque with camera frames for detector (queue 1)
        model_queue : deque
            deque with camera frames for recognizer (queue 2)
        input_step : int
            step size for frames to perform inference on
        detect_backup_pose : list 
            store incoming pose estimations for detector model 
        model_backup_pose : list
            store incoming pose estimations for recognizer model
        indx : int 
            index to keep track of how many frames have elapsed
        frames_after_last_model_inference : int
            index to keep track of how many frames have elapsed since running 
            (refractory period)
        trigger_model_queue : bool
            state variable to trigger the action recognition model queue collections
        model_queue_indx : int
            index with current number of frames collected
        text_out_time : float
            keep track of length of time text has been displayed with model output
        text_out : str
            text to display for model output info 

        """
        def __init__(self) -> None:
            ###################################################################
            # Pose Estimation #################################################
            config = os.path.join(project_dir, 'models/configs/mmpose', 
                DETECTOR_POSE_CONFIG_FILE
            )
            self.pose = Pose.Pose(config) 
            self.pose.kpt_thr = DEFAULT_KPT_THRESHOLD    
            self.pose.bbox_thr = DEFAULT_BBOX_THRESHOLD 
            self.render_pose = False
            ###################################################################
            # Detector and AR Model ###########################################
            self.detector = Detector.Detector(
                DEFAULT_DETECT_THRESHOLD, 
                model_config_path=detect_model_config_path,
                model_checkpoint_path=detect_model_checkpoint
            )
            self.detect_sample_length = self.detector.model.cfg.clip_len * \
                self.detector.model.cfg.num_clips
            self.model = Model.Model(model_config_path, model_checkpoint)
            self.score_thr = DEFAULT_SCORE_THRESHOLD
            self.action_sample_length = self.model.cfg.clip_len * \
                self.model.cfg.num_clips

            ### Model Queues and States ########################################
            self.detect_queue = deque(maxlen=self.detect_sample_length)
            self.model_queue = deque(maxlen=self.action_sample_length)
            self.detect_backup_pose = []
            self.model_backup_pose = []
            self.input_step = 1
            self.indx = 0 
            self.frames_after_last_model_inference = 0
            self.trigger_model_queue = False
            self.model_queue_indx = 0
            self.text_out_time = 0
            self.text_out = ''


        def _annotate(self, image: np.ndarray, text_in: str, type_in: str) -> bool:
            """ Annotate image with text.

            Args:
                image (np.ndarray): image to annotate
                text_in (str): text to annotate
                type_in (str): type of text (e.g. 'action')
            Returns:
                bool: True if text is annotated, False otherwise
            """

            display_text = text_in

            if type_in == 'upper_left':
                font_size = 0.7
                cv2.putText(
                    image, display_text, (15, 35),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    font_size, (0,), 2,
                )
            elif type_in == 'bottom_left':
                font_size = 0.5
                cv2.putText(
                    image, display_text, (15, 470),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    font_size, (255,255,255), 2,
                )
            elif type_in == 'upper_right':
                font_size = 0.7
                cv2.putText(
                    image, display_text, (400, 35),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    font_size, (0,), 2,
                )
            elif type_in == 'flash':
                image = np.ones_like(image)*255
            return image

        
        def recv(self, frame: av.VideoFrame) -> av.VideoFrame:
            """ Receive frame from camera and apply top-down logic with two queues.

            Args:
                frame (av.VideoFrame): frame received from camera
            Returns:
                av.VideoFrame: annotated frame
            """

            ### For FPS calculation
            time_0 = time.time()
            self.indx += 1 # keep track of how many input frames
            image = frame.to_ndarray(format="bgr24")
            ### Resize image
            # image = cv2.resize(image, (384, 288)) 
            ### Pose estimation
            pose_results, returned_outputs = self.pose.estimate_skeleton_of_img(image)
            
            if len(pose_results) > 0:
                kpts = np.expand_dims(pose_results[0]['keypoints'], axis=0)           
                self.detect_backup_pose.append(kpts)
                self.frames_after_last_model_inference += 1
                ### Detector 
                if self.indx == self.detect_sample_length:
                    self.detect_queue.extend(self.detect_backup_pose)
                    self.detect_backup_pose = []
                    self.frames_after_last_model_inference = 0
                if (len(self.detect_backup_pose) == self.input_step) and \
                    (self.indx > self.detect_sample_length):
                    ### Pick a frame from the backup
                    #### When the backup is full or reach the last frame
                    chosen_frame = random.choice(self.detect_backup_pose)
                    self.detect_queue.append(chosen_frame)
                    self.detect_backup_pose = []
                    if len(self.detect_queue) == self.detect_sample_length:
                        # Action detector to trigger and accumulate queue
                        detector_input = np.vstack(self.detect_queue)
                        if self.detector.solve_with_kpts(detector_input) and \
                            (self.frames_after_last_model_inference > DEFAULT_REFRACTORY_FRAME_PERIOD): 
                            self.trigger_model_queue = True
                            self.frames_after_last_model_inference = 0
                            # Make flash on trigger
                            image = self._annotate(image, "*Action*", 'flash')
                # AR Model
                if self.trigger_model_queue:
                    # Increase model queue until preset sample length
                    self.model_backup_pose.append(kpts)
                    # Display action on
                    image = self._annotate(image, "*Action*", 'upper_left')

                    if (len(self.model_backup_pose) == self.input_step) and \
                        (self.indx > self.action_sample_length):
                        chosen_frame = random.choice(self.model_backup_pose)
                        self.model_queue.append(chosen_frame)
                        self.model_queue_indx += 1
                        self.model_backup_pose = []

                    if self.model_queue_indx == self.action_sample_length:
                        model_input = np.vstack(self.model_queue)
                        model_output = self.model.inference(
                            model_input, img_shape=image.shape[0:2],
                        )
                        label, model_output_score = self.model.get_action_label_wscore_from_output()
                        if model_output_score >= self.score_thr:
                            self.text_out = label + ' - ' + str(round(model_output_score, 2))
                            self.text_out_time = time.time()
                        else:
                            self.text_out = 'None'
                            self.text_out_time = time.time()

                        # reset triggers and indices
                        self.trigger_model_queue = False # reset detector
                        self.model_queue_indx = 0 # reset the detector index
                        self.frames_after_last_model_inference = 0 # reset refractory period
            else: # reset to init state if no one is detected
                self.detect_backup_pose = []
                self.model_backup_pose = []
                self.indx = 0
            
            # End of inference - fps
            time_1 = time.time()
            fps = 1/(time_1-time_0)

            if time_1-self.text_out_time < TEXT_OUT_TIME:
                image = self._annotate(image, self.text_out, 'upper_right')
            
            img_result = self._annotate(image, 'FPS: ' + str(round(fps, 2)), 'bottom_left')
            if self.render_pose and len(pose_results) > 0:
                img_result = self.pose.vis_pose_result_of_img(img_result, pose_results)

            return av.VideoFrame.from_ndarray(img_result, format="bgr24")

    # Generate webrtc object with processor factory object
    webrtc_ctx = webrtc_streamer(
        key="action-recognition",
        mode=WebRtcMode.SENDRECV,
        rtc_configuration=RTC_CONFIGURATION,
        video_processor_factory=ARVideoProcessor,
        async_processing=True,
    )

    # Streamlit controls for models
    score_threshold = st.sidebar.slider(
        "Score threshold", DEFAULT_SCORE_THRESHOLD-0.5, 
        DEFAULT_SCORE_THRESHOLD+0.5, DEFAULT_SCORE_THRESHOLD, 0.05
    )
    detect_threshold = st.sidebar.slider(
        "Detection threshold", DEFAULT_DETECT_THRESHOLD-0.5, 
        DEFAULT_DETECT_THRESHOLD+0.5, DEFAULT_DETECT_THRESHOLD, 0.05
    )
    kpt_threshold = st.sidebar.slider(
        "Keypoint threshold", 0.0, 1.0, DEFAULT_KPT_THRESHOLD, 0.05
    )
    bbox_threshold = st.sidebar.slider(
        "Bounding box threshold", 0.0, 1.0, DEFAULT_BBOX_THRESHOLD, 0.05
    )
    ### Draw pose results
    render_pose = st.sidebar.checkbox("Draw skeletal overlay")
    if webrtc_ctx.video_processor:
        webrtc_ctx.video_processor.score_thr = score_threshold
        webrtc_ctx.video_processor.detector.detect_thr = detect_threshold
        webrtc_ctx.video_processor.pose.kpt_thr = kpt_threshold
        webrtc_ctx.video_processor.pose.bbox_thr = bbox_threshold
        webrtc_ctx.video_processor.render_pose = render_pose

if __name__ == "__main__":
    import os

    DEBUG = os.environ.get("DEBUG", "false").lower() not in ["false", "no", "0"]

    logging.basicConfig(
        format="[%(asctime)s] %(levelname)7s from %(name)s in %(pathname)s:%(lineno)d: "
        "%(message)s",
        force=True,
    )

    logger.setLevel(level=logging.DEBUG if DEBUG else logging.INFO)

    st_webrtc_logger = logging.getLogger("streamlit_webrtc")
    st_webrtc_logger.setLevel(logging.DEBUG)

    fsevents_logger = logging.getLogger("fsevents")
    fsevents_logger.setLevel(logging.WARNING)

    project_dir = Path(__file__).resolve().parents[2]

    main()
