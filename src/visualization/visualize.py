import numpy as np
import pandas as pd
from pathlib import Path
import os
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


def plot_confusion_matrix_isogd(df, filename):
    """plots confusion matrix given dataframe with
    columns [predicted_class,action_id,score]
    """
    import matplotlib.pyplot as plt
    import seaborn as sns
    c_size =df['action_id'].unique().shape[0]
    arr = np.zeros((c_size,c_size))
    df_cm = pd.DataFrame(arr, index=[i for i in df['action_id'].unique()], 
        columns=[i for i in df['action_id'].unique()])

    df = df.fillna(0)
    for index,row in df.iterrows():
        action_id = int(row['action_id'])
        yhat = int(row['predicted_class'])
        df_cm.at[action_id, yhat] += 1

    plt.figure(figsize = (10,7))
    fig = sns.heatmap(df_cm, annot=True).get_figure()
    plt.xlabel('Action Class')
    plt.ylabel('Action Class')
    plt.suptitle(filename)
    plt.tight_layout()

    if filename is not None:
        project_dir = Path(os.getcwd()).resolve().parents[1]
        plt.savefig(os.path.join(project_dir, 'reports/figures', filename))

    return fig

def plot_keypoint_summary(keypoints, filename=None):
    """plots keypoint summary:
        first derivative of keypoint scores (subplot 1)
        mean of keypoints score (subplot 2)
        std of keypoints score (subplot 3)
        std of x-keypoints (subplot 4)
        std of y-keypoints (subplot 5)
    Returns:
        None
    """
    # first deriv
    kpts_diff1 = np.diff(keypoints, axis=0)[:,:,-1]

    mean_scores = keypoints.mean(axis=0)[:,-1]
    std_scores = keypoints.std(axis=0)[:,-1]
    x_std = keypoints.std(axis=0)[:,0]
    y_std = keypoints.std(axis=0)[:,1]

    # plotting
    f,(ax0, ax1,ax2,ax3,ax4) = plt.subplots(5,1,sharex=True, figsize=(15,10), gridspec_kw={'height_ratios': [10,1,1,1,1]})


    g0 = sns.heatmap(kpts_diff1, ax=ax0)
    g0.set_title('first derivative of keypoints')
    g0.set_ylabel('frame-1')
    df = pd.DataFrame(np.expand_dims(mean_scores, axis=1).T, index=[''])
    g1 = sns.heatmap(df,cmap="inferno", cbar=True,ax=ax1)
    g1.set_ylabel('mean-score')
    df = pd.DataFrame(np.expand_dims(std_scores, axis=1).T, index=[''])
    g2 = sns.heatmap(df,cmap="inferno", cbar=True,ax=ax2)
    g2.set_ylabel('std-score')
    df = pd.DataFrame(np.expand_dims(x_std, axis=1).T, index=[''])
    g3 = sns.heatmap(df,cmap="inferno", cbar=True,ax=ax3)
    g3.set_ylabel('x-std')
    df = pd.DataFrame(np.expand_dims(y_std, axis=1).T, index=[''])
    g4 = sns.heatmap(df,cmap="inferno", cbar=True,ax=ax4)
    g4.set_ylabel('y-std')
    g4.set_xlabel('keypoints')
    f.suptitle('Pose Kinematics Summary')
    plt.tight_layout()
    if filename is not None:
        project_dir = Path(os.getcwd()).resolve().parents[0]
        plt.savefig(os.path.join(project_dir, 'reports/figures', filename))
    return 
