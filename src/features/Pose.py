import os
from dotenv import find_dotenv, load_dotenv
try:
    from mmdet.apis import inference_detector, init_detector
    has_mmdet = True
except (ImportError, ModuleNotFoundError):
    has_mmdet = False

from mmpose.apis import (inference_top_down_pose_model, init_pose_model,
                         vis_pose_result)

import pydload
from pathlib import Path
from timeit import default_timer as timer

# Local imports
import src.data.utils as data_utils


class Pose(object):
    """Creates a pose model object from MMCV

    Args:
        config_file (str): path to the config file
    """
    def __init__(self, config_file):
        project_dir = Path(__file__).resolve().parents[2]
        # Load pose configuration variables in directory
        # checkpoint_file (str): path to the checkpoint file
        # device (str): device to run pose estimation on
        # pose_config_name (str): name of pose config
        # dataset (str): dataset to run pose estimation on
        load_dotenv(
            os.path.join(project_dir, 'models/configs/mmpose', config_file), 
            override=True
        )
        # Device to run pose estimation
        self.device = os.getenv('DEVICE')
        # Name of pose config
        self.pose_config_name = os.getenv("POSECONFIGNAME")

        # build the pose model from a config file and a checkpoint file
        self.pose_config = os.path.join(
            project_dir, 'models/configs/mmpose', os.getenv('POSE_CONFIG')
        )
        self.pose_checkpoint = os.path.join(
            project_dir, 'models/configs/mmpose', os.getenv('POSE_CHECKPOINT')
        )
        # If model checkpoint isn't downloaded, do so..
        if not os.path.exists(self.pose_checkpoint):
            checkpoint_url = os.getenv("POSE_CHECKPOINT_URL")
            print(checkpoint_url)
            pydload.dload(checkpoint_url, save_to_path=self.pose_checkpoint, max_time=None) 

        self.pose_model = init_pose_model(
            self.pose_config, self.pose_checkpoint, device=self.device.lower()
        )
        # assign model type, e.g., top-down, bottom-up
        self.pose_model_type = self.pose_model.cfg.model['type']
        self.dataset = self.pose_model.cfg.data['test']['type']
        
        # init keypoint dimension
        self.kp_dim = self.pose_model.cfg.channel_cfg['num_output_channels']
        self.heatmap_size = None
        if self.pose_model_type == 'TopDown':
            assert has_mmdet, 'Please install mmdet to run.'
            # Configs for human detection (top-down)
            self.det_config = os.path.join(
                project_dir, 'models/configs/mmdet', os.getenv('DET_CONFIG')
            )
            self.det_checkpoint = os.path.join(
                project_dir, 'models/configs/mmdet', os.getenv('DET_CHECKPOINT')
            )
            if not os.path.exists(self.det_checkpoint):
                checkpoint_url = os.getenv("DET_CHECKPOINT_URL")
                pydload.dload(checkpoint_url, save_to_path=self.det_checkpoint, max_time=None)

            self.bbox_thr = float(os.getenv('BBOX_THR')) # Bounding box score threshold
            self.det_cat_id = int(os.getenv('DET_CAT_ID')) # Category id for bounding box detection model
            self.kpt_thr = float(os.getenv('KPT_THR')) # Keypoint score threshold
            self.output_layer_names = os.getenv('OUTPUT_LAYER_NAMES')
            if self.output_layer_names == 'None':
                self.output_layer_names = None
            self.return_heatmap = bool(int(os.getenv('RETURN_HEATMAP')))

            self.heatmap_size = self.pose_model.cfg.data_cfg['heatmap_size']
            # self.heatmap_size = (self.heatmap_size[1],self.heatmap_size[0])

            #assert args.show or (args.out_video_root != '')
            assert self.det_config is not None
            assert self.det_checkpoint is not None


            # build detection model from a config file and a checkpoint file
            self.det_model = init_detector(
                self.det_config, self.det_checkpoint, device=self.device.lower()
            )

    def _process_mmdet_results(self, mmdet_results, cat_id=1):
        """Process mmdet results, and return a list of bboxes.

        Args:
            mmdet_results: mmdet results
            cat_id (int, optional): category id for bounding box detection model. Defaults to 1.
        Returns:
            person_results [list]:  a list of detected bounding boxes

        """
        if isinstance(mmdet_results, tuple):
            det_results = mmdet_results[0]
        else:
            det_results = mmdet_results

        bboxes = det_results[cat_id - 1]

        person_results = []
        for bbox in bboxes:
            person = {}
            person['bbox'] = bbox
            person_results.append(person)

        return person_results

    def vis_pose_result_of_img(self, img, pose_results, out_file=None):
        """Visualize pose results on an image.
        
        Args:
            img (np.array): image to visualize pose results on
            pose_results (list): list of pose results
            out_file (str, optional): path to output file. Defaults to None.

        Returns:
            vis_img (np.array): image with pose results visualized
        """
        # write results to image
        if self.pose_model_type == 'TopDown':
            vis_img = vis_pose_result(
                self.pose_model,
                img,
                pose_results,
                dataset=self.dataset,
                kpt_score_thr=self.kpt_thr,
                show=False,
                out_file=out_file)
        else: vis_img = None
        return vis_img


    def detect_person(self, img):
        """Returns bounding box of person

        Args:
            img (np.array): image to detect person on
        Returns:
            person_results (list): list of detected bounding boxes
        """
        if self.pose_model_type == 'TopDown':
            # test a single image, the resulting box is (x1, y1, x2, y2)
            mmdet_results = inference_detector(self.det_model, img)
            # keep the person class bounding boxes.
            person_results = self._process_mmdet_results(mmdet_results, self.det_cat_id)
        else:
            person_results = None
        return person_results

    def estimate_skeleton_of_img(self, img):
        """Returns pose skeleton estimate from initialized model

        Args:
            img (np.array): image to estimate skeleton on

        Returns:
            pose_results (dict): pose skeleton estimate
            returned_outputs (list)

        """
        if self.pose_model_type == 'TopDown':
            # get bounding box of person
            person_results = self.detect_person(img)
            # single image, with a list of bboxes.
            pose_results, returned_outputs = inference_top_down_pose_model(
                self.pose_model,
                img,
                person_results,
                bbox_thr=self.bbox_thr,
                format='xyxy',
                dataset=self.dataset,
                return_heatmap=self.return_heatmap,
                outputs=self.output_layer_names)
        else:
            pose_results, returned_outputs = None, None
        return pose_results, returned_outputs
