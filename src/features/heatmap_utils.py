import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
from mmpose.apis import vis_pose_result
from mmpose.models import TopDown
from mmcv import load, dump
import matplotlib.cm as cm
from mmaction.datasets.pipelines import Compose

def vis_heatmaps(heatmaps, channel=-1, ratio=8):
    # if channel is -1, draw all keypoints / limbs on the same map
    
    h, w, _ = heatmaps[0].shape
    newh, neww = int(h * ratio), int(w * ratio)
    
    if channel == -1:
        heatmaps = [np.max(x, axis=-1) for x in heatmaps]
    cmap = cm.viridis
    heatmaps = [(cmap(x)[..., :3] * 255).astype(np.uint8) for x in heatmaps]
    heatmaps = [cv2.resize(x, (neww, newh)) for x in heatmaps]
    return heatmaps

def generate_heatmap_from_keypoint(keypoints, orig_img_size, selected_kps=None):
    # sigma : exp(−[(i−x_k)^2+ (j−y_k)^2]/(2*sigma^2))*c_k
    ######### KEYPOINT PIPELINE ############################
    keypoint_pipeline = [
        dict(type='PoseDecode'),
        dict(type='PoseCompact', hw_ratio=1., allow_imgpad=True),
        dict(type='Resize', scale=(-1, 64)),
        dict(type='CenterCrop', crop_size=64),
        dict(type='GeneratePoseTarget', sigma=0.6, use_score=True, with_kp=True, with_limb=False)
    ]
    #########################################################

    # Generate keypoint heatmap from pipeline and keypoints
    results = dict()
    results['keypoint'] = np.expand_dims(keypoints[:,selected_kps,0:2], axis=0) # keypoint
    results['keypoint_score'] = np.expand_dims(keypoints[:,selected_kps,-1], axis=0) # keypoint score
    results['total_frames'] = keypoints.shape[0]
    results['img_shape'] = orig_img_size

    # Execute pipeline
    pipeline = Compose(keypoint_pipeline)
    return pipeline(results)