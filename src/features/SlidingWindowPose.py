
from src.data import Reader
from src.features import Pose

import numpy as np

class SlidingWindowPoseFromVideo(Reader.SlidingWindowFromVideo):
    """Subclass for generating pose information from 
    dynamic sliding window over video or live stream"""
    def __init__(self, config_filepath, video_path, window_size=30, 
        sample_interval=1, window_sample_interval=1
        ):
        super(SlidingWindowPoseFromVideo, self).__init__(
            video_path, window_size, sample_interval=sample_interval, 
            window_sample_interval=window_sample_interval
        )
        self.pose = Pose.Pose(config_filepath)
        self._skeleton_window_frames = []

        # initialize skeleton array
        self._skeleton_arr = np.empty((self.window_size,self.pose.body_kp_dim,3))

        self.track_indx = 0 # always select first index NO TRACKING IMPLEMENTED

    def read_window(self):
        # init window 
        if len(self._window_frames) == 0:
            for i in range(self.window_size):
                img = self.read_image()
                # process image
                pose_results,_ = self.pose.estimate_skeleton_of_img(img)
                self._skeleton_window_frames.append(pose_results)
                self._window_frames.append(img)

                self._skeleton_arr[i] = pose_results[self.track_indx]['keypoints']
        # remove last frames and update next frames
        else:
            del self._window_frames[0:self.window_sample_interval]
            del self._skeleton_window_frames[0:self.window_sample_interval]
            # update np array
            self._skeleton_arr = np.roll(self._skeleton_arr, 
                -self.window_sample_interval
            )
            for i in range(self.window_sample_interval):
                img = self.read_image()
                # process image
                pose_results,_ = self.pose.estimate_skeleton_of_img(img)
                self._skeleton_window_frames.append(pose_results)
                self._window_frames.append(img)
                # update np array
                self._skeleton_arr[-self.window_sample_interval+i] = \
                    pose_results[self.track_indx]['keypoints']

        return self._skeleton_window_frames, self._skeleton_arr