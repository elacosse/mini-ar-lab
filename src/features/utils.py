# Feature utilities
from logging import raiseExceptions
import numpy as np
from scipy.signal import medfilt
from scipy.ndimage.filters import gaussian_filter
from scipy.signal import savgol_filter
import pandas as pd

CONDENSED_KPTS = np.concatenate( # 27 joints
    ([0,5,6,7,8,9,10], # neck, l_shoulder, r_shoulder, l_elbow, r_elbow, l_wrist, r_wrist
     [91,95,96,99,100,103,104,107,108,111], # condensed right hand
     [112,116,117,120,121,124,125,128,129,132]), # condensed  left hand
    axis=0
)
CONDENSED_KPTS_LABELS = ['neck', 'l_shoulder', 'r_shoulder', 'l_elbow', 'r_elbow',\
    'l_wrist', 'r_wrist', 'r_hand-1', 'r_hand-2', 'r_hand-3', 'r_hand-4',\
    'r_hand-5','r_hand-6','r_hand-7','r_hand-8','r_hand-9','r_hand-10', 'l_hand-1',\
    'l_hand-2','l_hand-3','l_hand-4','l_hand-5','l_hand-6','l_hand-7','l_hand-8',\
    'l_hand-9','l_hand-10']

def calc_bones_wscore(kpts, body_type='wbcoco-27'):
    if body_type == 'wbcoco-27':
        sel_bones = (
            (5, 6), (5, 7),
            (6, 8), (8, 10), (7, 9), (9, 11), 
            (12,13),(12,14),(12,16),(12,18),(12,20),
            (14,15),(16,17),(18,19),(20,21),
            (22,23),(22,24),(22,26),(22,28),(22,30),
            (24,25),(26,27),(28,29),(30,31),
            (10,12),(11,22)
        )
        bones = np.zeros((kpts.shape[0], len(sel_bones),3))
        for i, kp in enumerate(sel_bones):
            kp1 = kp[0]-5
            kp2 = kp[1]-5
            bones[:,i,0:2] = kpts[:,kp2,0:2]-kpts[:,kp1,0:2]
            # take average of points for score
            bones[:,i,-1] = kpts[:,kp2,-1]+kpts[:,kp1,-1]/2
    return bones

def calc_pose_vel(kpts):
    """Calculates the pose velocity 
    :param kpts - normalized keypoints (T x XY)
    """
    diff = np.zeros_like(kpts, dtype='float32')
    diff[1:] = np.diff(kpts, axis=0)
    return diff

def calc_pose_angle_vel(angles):
    """
    :param kpts - normalized keypoints (T x XY)
    """
    diff = np.zeros_like(angles, dtype='float32')
    diff[1:] = np.diff(angles, axis=0)
    return diff

def calc_norm_angles(kpts):
    """calculate normalized angles.
    In order to get a positive angle between 0 and 2π 
    take the modulo against 2π.
    :params kpts - normalized kpts (T x pt X dim"""
    angles = np.arctan2(kpts[:,:,1], kpts[:,:,0]) % (2 * np.pi) 
    return angles


def calc_norm_angle_diff(v1, v2):
    """subtract the second angle from the first to get the signed
     counter-clockwise angular difference. 
     This difference will be between -2π and 2π, 
     so in order to get a positive angle between 0 and 2π 
     take the modulo against 2π.
    """
    angle1 = np.arctan2(*v1[::-1])
    angle2 = np.arctan2(*v2[::-1])
    return (angle2 - angle1) % (2 * np.pi)



def normalize_skeleton_by_com(keypoint, body_indices=list(range(0, 18)),
        scaling_bodypart='shoulder'):
    """Normalizes skeleton making it translational and scale invariant.
    Translation: center of mass
    Scale: shoulder width
    """
    assert(keypoint[0,0] != np.nan)

    if scaling_bodypart == 'shoulder':
        scaling_kp_indx_1 = 5 # right shoulder
        scaling_kp_indx_2 = 6 # left shoulder
    scaling_kp_1 = keypoint[scaling_kp_indx_1]
    scaling_kp_2 = keypoint[scaling_kp_indx_2]
    dist = np.abs(scaling_kp_1[0:2]-scaling_kp_2[0:2]) # maybe check body orientation via without abs?
    norm_dist = np.linalg.norm(dist) 

    # select 17 body parts for COM calculation
    keypoint_body = keypoint[body_indices, 0:2] # coco 17 body
    xy_bar = np.array([np.mean(keypoint_body[:,0]), 
        np.mean(keypoint_body[:,1])])
    trans_kp = xy_bar
    norm_keypoint = np.copy(keypoint)
    for kp_indx in range(len(keypoint)):
        norm_keypoint[kp_indx,0:2] = (keypoint[kp_indx,0:2]-trans_kp)/norm_dist
    return norm_keypoint

def normalize_skeleton_by_bbox(results):
    """Normalizes skeleton making it translational and scale invariant.
    Translation: center of bounding box 
    Scale: height of bounding box
    """
    # for every timepoint
    norm_results = []
    for t_indx in range(len(results)):
        bbox = results[t_indx]['bbox']
        keypoints = results[t_indx]['keypoints']
        norm_keypoints = keypoints.copy()
        if keypoints[0,0] != np.nan: # if skeleton was found
            x_center = (bbox[0] + bbox[2])/2
            y_center = (bbox[1] + bbox[3])/2
            trans_kp = (x_center, y_center)
            # translate to center of bbox, scale to height of bbox
            norm_dist = np.abs(bbox[3] - bbox[1])
            for kp_indx in range(len(keypoints)):
                norm_keypoints[kp_indx,0:2] = (keypoints[kp_indx,0:2]-trans_kp)/norm_dist
            norm_results.append(norm_keypoints)
    return norm_results

def normalize_wbcoco_skeleton(keypoints, translation_bodypart='neck',
    scaling_bodypart='shoulder', zero_mean=True, unit_var=False):
    """Normalizes skeleton making it translational and scale invariant.
    Following normalization, the signals are standardize (zero mean or unit variance).
    Translation: distance to neck [0]
    Scaling: normalize by shoulder width size [7, 6] (left, right) (COCO WholeBody)
    
    Keyword arguments:
    keypoints -- keypoints from body model (t, 133, 3) [time x keys x XYscore] 
    zero_mean -- standadize to zero mean (boolean) [default is True]
    unit_var -- standardize to unit variance (boolean) [default is False]

    TODO:
        check if series has no nans from missing frames in video
        normalize according to different translation and scaling body parts,
        e.g., length of body and center of gravity
    """
    assert len(keypoints) > 0
    #assert len(bboxes) == len(keypoints) # bbox -- bounding box (t, 5) [time x X1X2Y1Y2score]

    ####### Normalize ###########
    # select keypoint location for normalization
    if translation_bodypart == 'neck':
        translation_kp_indx = 0 # neck
    if scaling_bodypart == 'shoulder':
        scaling_kp_indx_1 = 5 # right shoulder
        scaling_kp_indx_2 = 6 # left shoulder

    # for every timepoint
    norm_keypoints = np.nan_to_num(keypoints)
    #norm_keypoints = np.zeros_like(keypoints)
    for t_indx in range(len(norm_keypoints)):
        # if keypoints[t_indx,0,0] != np.nan: # if skeleton was found
        trans_kp = keypoints[t_indx][translation_kp_indx][0:2]
        scaling_kp_1 = keypoints[t_indx][scaling_kp_indx_1]
        scaling_kp_2 = keypoints[t_indx][scaling_kp_indx_2]
        dist = np.abs(scaling_kp_1[0:2]-scaling_kp_2[0:2]) # maybe check body orientation via without abs?
        norm_dist = np.linalg.norm(dist) 
        # for every keypoint
        for kp_indx in range(len(keypoints[t_indx,])):
            s = keypoints[t_indx,kp_indx,0:2]-trans_kp
            s /= norm_dist
            norm_keypoints[t_indx,kp_indx,0:2] = s
    return norm_keypoints

def remove_t_samples_below_threshold(kpts, kpts_score, thr = 0.3, kps_idx=CONDENSED_KPTS):
    # for every timepoint
    if kps_idx is not None:
        sel_kpts_score = kpts_score[:,:,kps_idx]
    new_keypoints = []
    new_scores = []
    #norm_keypoints = np.zeros_like(keypoints)
    for t_indx in range(kpts.shape[1]):
        if not np.sum(sel_kpts_score[0,t_indx] < thr):
            new_keypoints.append(kpts[:,t_indx,:])
            new_scores.append(kpts_score[:,t_indx,:])
    if len(new_keypoints) > 0:
        return np.expand_dims(np.vstack(new_keypoints), axis=0), np.expand_dims(np.vstack(new_scores), axis=0)
    else:
        return None,None


def apply_filter(keypoints, filter='sav', zero_mean=True, unit_var=False):
    """Applies filter to the keypoints and standardizes
    
    Args:
        keypoints : (t, kp_dim, xy_dim, score)
    """

    keypoints = np.nan_to_num(keypoints)

    ####### Filter ###########
    if filter == 'med':
        kernel_size = 5
        for kp_dim in range(0, keypoints.shape[1]):
            for xy_dim in range(0, keypoints.shape[2]-1):
                keypoints[:,kp_dim, xy_dim] = medfilt(keypoints[:,kp_dim, xy_dim], kernel_size)

    if filter == 'gauss':
        kernel_size = 1
        for kp_dim in range(0, keypoints.shape[1]):
            for xy_dim in range(0, keypoints.shape[2]-1):
                keypoints[:,kp_dim, xy_dim] = gaussian_filter(keypoints[:,kp_dim, xy_dim], kernel_size)

    if filter == 'sav':
        window_length = 5
        polyorder = 2
        for kp_dim in range(0, keypoints.shape[1]):
            for xy_dim in range(0, keypoints.shape[2]-1):
                keypoints[:,kp_dim, xy_dim] = savgol_filter(keypoints[:,kp_dim, xy_dim], window_length, polyorder)
    

    ####### Standardize ###########
    if zero_mean:
        keypoints[:,:,0:2] = keypoints[:,:,0:2] - keypoints[:,:,0:2].mean(axis=0)
    if unit_var:
        np.seterr(divide='ignore', invalid='ignore')
        keypoints[:,:,0:2] = keypoints[:,:,0:2]/keypoints[:,:,0:2].std(axis=0)
        keypoints = np.nan_to_num(keypoints)

    return keypoints

def get_keypoints_summary_stats(keypoints):
    """Returns stat keypoint summary as summary DataFrame"""
    dfs = [pd.DataFrame(keypoints[:,kp_indx,:], columns=['x', 'y', 'score']).describe() \
        for kp_indx in range(keypoints.shape[1])]
    df = pd.concat(dfs, keys=range(keypoints.shape[1]))
    return df

def get_selected_kp_dims_above_thr(keypoints, thr=0.0):
    """Returns list of selected keypoint dimensions above threshold"""

    x_std = np.nanstd(keypoints, axis=0)[:,0]
    y_std = np.nanstd(keypoints, axis=0)[:,1]

    sorted_x_std = [(x,y) for x,y in zip(np.sort(x_std), np.argsort(x_std))]
    sorted_y_std = [(x,y) for x,y in zip(np.sort(y_std), np.argsort(y_std))]

    # take dims where std is above threshold for either x,y dimension
    if isinstance(thr, float):
        selected_kp_dims = set([kp[1] for kp in sorted_x_std if kp[0] > thr])
        selected_kp_dims.union([kp[1] for kp in sorted_y_std if kp[0] > thr])
    elif isinstance(thr, int): # take top 12
        try:
            selected_kp_dims = set([kp[1] for kp in sorted_x_std[-thr:]])
            selected_kp_dims.union([kp[1] for kp in sorted_y_std[-thr:]])
        except Exception as e:
            raiseExceptions(e)

    return selected_kp_dims


def np_to_pose_results(keypoints, bbox):
    """convert to original pose results return by model"""
    pose_results = [{'keypoints': keypoints,
                     'bbox': bbox}]
    return pose_results


def arr_scale(X, x_min, x_max):
    """Scale matrix between min and max
    """
    nom = (X-X.min(axis=0))*(x_max-x_min)
    denom = X.max(axis=0) - X.min(axis=0)
    denom[denom==0] = 1
    return x_min + nom/denom 

def compute_cov_and_flatten(keypoints, selected_dims=None, include_scores=True, scale='0-1'):
    """Data    encoding    through    covariance    representation
    keypoints (t, k-dim, (x,y,score))
    """
    # selected dimensions, score
    if selected_dims is None: # select all
        selected_dims = list(range(keypoints.shape[1]))
    X = keypoints[:,selected_dims,0][~np.isnan(keypoints[:,selected_dims,0]).any(axis=1)]
    Y = keypoints[:,selected_dims,1][~np.isnan(keypoints[:,selected_dims,1]).any(axis=1)]
    S = keypoints[:,selected_dims,2][~np.isnan(keypoints[:,selected_dims,2]).any(axis=1)]
    if include_scores:
        cov = np.cov(np.concatenate([X,Y,S], axis=1).T)
    else: 
        cov = np.cov(np.concatenate([X,Y], axis=1).T)

    # scale
    if scale == '0-1':
        cov = arr_scale(cov,0,1)

    # extract upper tri 
    uptril = cov[np.triu_indices(cov.shape[0], k=0)]
    return uptril



def calc_joint_angle(a,b,c):
    """Calculates the angle for 3 points"""
    dp1 = a-b
    dp2 = c-b
    if np.linalg.norm(a)*np.linalg.norm(b)*np.linalg.norm(c) == 0:
        return 0
    else:
        res = dp1.dot(b)/(np.linalg.norm(a)*np.linalg.norm(b))
        return res

def euclidean_dist(a, b):
    # This function calculates the euclidean distance between 2 point in 2-D coordinates
    # if one of two points is (0,0), dist = 0
    # a, b: input array with dimension: m, 2
    # m: number of samples
    # 2: x and y coordinate

    if (a.shape[1] == 2 and a.shape == b.shape):
        # check if element of a and b is (0,0)
        bol_a = (a[:,0] != 0).astype(int)
        bol_b = (b[:,0] != 0).astype(int)
        dist = np.linalg.norm(a-b, axis=1)
        return((dist*bol_a*bol_b).reshape(a.shape[0],1))
    else:
        # Error: Check dimension of input vector
        return 0