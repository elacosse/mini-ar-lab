from mmaction.datasets import PIPELINES
import numpy as np

########################################################################################
# Extra data pipeline routines for mmaction model data handling
########################################################################################

@PIPELINES.register_module()
class SubtractIndexFromLabel:
    def __call__(self, results):
        results['label'] = results['label']-1
        return results

from src.features.utils import CONDENSED_KPTS
@PIPELINES.register_module()
class SelectCondensedHandsAndArms:
    def __call__(self, results):        
        results['keypoint'] = results['keypoint'][:,:,CONDENSED_KPTS]
        results['keypoint_score'] = results['keypoint_score'][:,:,CONDENSED_KPTS]
        return results

@PIPELINES.register_module()
class NanToZeros:
    def __call__(self, results):      
        results['keypoint'] = np.nan_to_num(
            results['keypoint']
        )
        results['keypoint_score'] = np.nan_to_num(
            results['keypoint_score']
        )
        return results

@PIPELINES.register_module()
class GetMiddleFrame:
    def __call__(self, results):      
        middle_frame_index = int(results['total_frames']/2)
        results['keypoint'] = results['keypoint'][:,middle_frame_index:middle_frame_index+1]
        results['keypoint_score'] = results['keypoint_score'][:,middle_frame_index:middle_frame_index+1]
        results['total_frames'] = 1
        return results

from src.features.utils import normalize_wbcoco_skeleton
@PIPELINES.register_module()
class NormalizeSkeleton:
    def __call__(self, results, norm_type='wbcoco'): 
        if norm_type == 'wbcoco':       
            results['keypoint'][0,:] = normalize_wbcoco_skeleton(results['keypoint'][0])
        return results

@PIPELINES.register_module()
class FormatKPAndScore:
    def __call__(self, results, wscore=True):
        kp = results['keypoint']
        if wscore == True:
            score = results['keypoint_score']
            results['pose'] = np.hstack((kp.reshape(1,-1), score.reshape(1, -1)))
        else:
            results['pose'] = kp.reshape(1,-1)
        return results