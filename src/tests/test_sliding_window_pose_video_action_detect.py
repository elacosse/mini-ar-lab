# -*- coding: utf-8 -*-
import pytest
from pathlib import Path
import os
project_dir = Path(__file__).resolve().parents[2]
from src.features import SlidingWindowPose
import src.features.utils as feature_utils

import time
project_dir = Path(__file__).resolve().parents[2]
video_path = os.path.join(project_dir, 'data/examples/videos/continuous/1.avi')
config_filepath = os.path.join(project_dir, 'models', 'wbcoco_mmpose_config.yml')

def test_sliding_window_pose_video():
    save_dir = os.path.join(project_dir, 
        'src/tests/.test_plots')
    Path(save_dir).mkdir(parents=False, exist_ok=True)

    # sliding window pose reader
    reader = SlidingWindowPose.SlidingWindowPoseFromVideo(
        config_filepath, video_path, window_size=40,
        sample_interval=1, window_sample_interval=20
    )

    # read in window
    f,kpts = reader.read_window()
    assert kpts[10] == f[10][0]['keypoints']
    assert kpts[-1] == f[-1][0]['keypoints']

    # preprocess
    # normalize, demean and filter
    kpts_norm = feature_utils.normalize_wbcoco_skeleton(kpts)
    kpts_norm_filt = feature_utils.apply_filter(kpts_norm, filter='med')


    # send through AR net
    

    # get kp dims above threshold
    thr = 0.5
    kpts_sel_dim = feature_utils.get_selected_kp_dims_above_thr(kpts, thr)

