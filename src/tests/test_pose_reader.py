# test_reader.py

import pytest
from pathlib import Path
import os
project_dir = Path(__file__).resolve().parents[2]
from src.data import PoseReader
import numpy as np
folder_path = 'data/examples/videos/processed/M_00001'
folder_path = os.path.join(project_dir, folder_path)
config_filepath = os.path.join(project_dir, 'models', 'wbcoco_mmpose_config.yml')
def test_ReadFromFolder():
    reader = PoseReader.ReadFromFolder(folder_path, config_filepath)
    img = reader.read_image()
    assert img.shape == (240, 320, 3)
    for i in range(0, 10):
        reader.read_keypoint()


def test_SlidingWindowFromVideo():
    reader = PoseReader.SlidingWindowFromVideo(folder_path, config_filepath,
        40, 1, 1
    )
    window_frames, keypoints = reader.read_window()
    assert len(keypoints) == 40

    window_frames, keypoints_1 = reader.read_window()
    window_frames, keypoints_2 = reader.read_window()
    compare = keypoints_1 == keypoints_2
    assert not compare.all()

    # moving window by 1 frame
    window_frames, keypoints_1 = reader.read_window()
    window_frames, keypoints_2 = reader.read_window()
    compare = keypoints_1[1:11] == keypoints_2[0:10]
    assert compare.all()

    # run to end of frames to return nan
    reader = PoseReader.SlidingWindowFromVideo(folder_path, config_filepath,
        40, 1, 20
    )
    for i in range(200):
        kp = reader.read_keypoint()
    assert np.isnan(kp)

    # check if every other is the same from different sample interval
    reader = PoseReader.SlidingWindowFromVideo(folder_path, config_filepath,
        40, 2, 5
    )
    window_frames, keypoints_2 = reader.read_window()

    reader = PoseReader.SlidingWindowFromVideo(folder_path, config_filepath,
        40, 1, 5
    )
    window_frames, keypoints_1 = reader.read_window()
    assert keypoints_1[0,0,0] == keypoints_2[0,0,0]
    assert keypoints_1[1,0,0] != keypoints_2[1,0,0]
    assert keypoints_1[2,0,0] == keypoints_2[1,0,0]
    assert keypoints_1[4,0,0] == keypoints_2[2,0,0]