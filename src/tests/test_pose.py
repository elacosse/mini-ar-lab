# -*- coding: utf-8 -*-
import pytest
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import os 
import cv2
import time
from src.features.Pose import Pose
import matplotlib.pyplot as plt

from mmdet.apis import (async_inference_detector, inference_detector,
                        init_detector, show_result_pyplot)
# show_result_pyplot(model, args.img, result[0], score_thr=args.score_thr)


project_dir = Path(__file__).resolve().parents[2]
img_filepath = os.path.join(project_dir, 'data/examples/images', 'single_person_1.jpg')
video_path = os.path.join(project_dir, 'data/examples/videos/continuous/1.avi')

def test_pose_single_person_img():
    save_dir = os.path.join(project_dir, 
        'src/tests/.test_plots')
    Path(save_dir).mkdir(parents=False, exist_ok=True)

    config_filepath = os.path.join(project_dir, 'models/configs/mmpose/wbcoco_mmpose_config.yml')
    pose = Pose(config_filepath)

    img = cv2.imread(img_filepath)
    pose_results, returned_outputs = pose.estimate_skeleton_of_img(img)
    out_file = os.path.join(save_dir, 'test_pose_single_person_1.jpg')
    vis_img = pose.vis_pose_result_of_img(img, pose_results, out_file)

    cap = cv2.VideoCapture(video_path)
    assert cap.isOpened(), f'Faild to load video file {video_path}'

    start = time.time()
    while (cap.isOpened()):
        flag, img = cap.read()
        if not flag:
            break
        pose_results, returned_outputs = pose.estimate_skeleton_of_img(img)
    cap.release()
    stop = time.time()

    for i in range(0, 20):
        start = time.time()
        while (cap.isOpened()):
            flag, img = cap.read()
            if not flag:
                break
            pose_results, returned_outputs = pose.estimate_skeleton_of_img(img)
        cap.release()
        stop = time.time()

if __name__ == '__main__':
    test_pose_single_person_img()