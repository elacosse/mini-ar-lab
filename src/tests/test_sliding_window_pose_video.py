# -*- coding: utf-8 -*-
import pytest
from pathlib import Path
import os
project_dir = Path(__file__).resolve().parents[2]
from src.features import SlidingWindowPose
import time
project_dir = Path(__file__).resolve().parents[2]
video_path = os.path.join(project_dir, 'data/examples/videos/continuous/1.avi')
config_filepath = os.path.join(project_dir, 'models', 'wbcoco_mmpose_config.yml')

def test_sliding_window_pose_video():
    save_dir = os.path.join(project_dir, 
        'src/tests/.test_plots')
    Path(save_dir).mkdir(parents=False, exist_ok=True)

    reader = SlidingWindowPose.SlidingWindowPoseFromVideo(
        config_filepath, video_path, window_size=40,
        sample_interval=1, window_sample_interval=20
    )

    start =time.time()
    s = reader.read_window()
    stop = time.time()
    print(len(s), stop-start)

    start =time.time()
    s = reader.read_window()
    stop = time.time()
    print(len(s), stop-start)

    start =time.time()
    s = reader.read_window()
    stop = time.time()
    print(len(s), stop-start)
    del reader
    reader = SlidingWindowPose.SlidingWindowPoseFromVideo(
        config_filepath, video_path, window_size=40,
        sample_interval=1, window_sample_interval=10
    )

    start =time.time()
    s = reader.read_window()
    stop = time.time()
    print(len(s), stop-start)

    start =time.time()
    s = reader.read_window()
    stop = time.time()
    print(len(s), stop-start)

    start =time.time()
    s = reader.read_window()
    stop = time.time()
    print(len(s), stop-start)
    # pose_results, returned_outputs = pose.estimate_skeleton_of_img(img)
    
    # out_file = os.path.join(save_dir, 'test_pose_single_person_1.jpg')
    # vis_img = pose.vis_pose_result_of_img(img, pose_results, out_file)







