# test_reader.py

import pytest
from pathlib import Path
import os
project_dir = Path(__file__).resolve().parents[2]
from src.data import Reader
import numpy as np
video_path = os.path.join(project_dir, 'data/examples/videos/continuous/1.avi')

def test_ReadFromVideo():
    reader = Reader.ReadFromVideo(video_path)
    img = reader.read_image()
    assert img.shape == (240, 320, 3)

    img1 = reader.read_image()
    img2 = reader.read_image()
    assert not (img1==img2).all()


def test_SlidingWindowFromVideo():
    reader = Reader.SlidingWindowFromVideo(video_path, 40, 1, 1)
    window_frames = reader.read_window()
    assert len(window_frames) == 40

    window_frames = reader.read_window()
    img_1 = window_frames[0]
    window_frames = reader.read_window()
    img_2 = window_frames[0]
    compare = img_1 == img_2
    assert not compare.all()

    # moving window by 1 frame
    window_frames = reader.read_window()
    img_1 = window_frames[1]
    window_frames = reader.read_window()
    img_2 = window_frames[0]
    compare = img_1 == img_2
    assert compare.all()

    # moving window by 20 frames
    reader = Reader.SlidingWindowFromVideo(video_path, 40, 1, 20)
    window_frames = reader.read_window()
    assert len(window_frames) == 40

    window_frames = reader.read_window()
    img_1 = window_frames[20]
    window_frames = reader.read_window()
    img_2 = window_frames[0]
    compare = img_1 == img_2
    assert compare.all()