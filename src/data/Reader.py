import os
import warnings
import numpy as np
import cv2
import time
import glob
import threading
import queue
import multiprocessing

import os
from dotenv import find_dotenv, load_dotenv



from pathlib import Path
from timeit import default_timer as timer

class ReadFromVideo(object):
    def __init__(self, video_path, sample_interval=1):
        ''' A video reader class for reading video frames from video.
        Arguments:
            video_path
            sample_interval {int}: sample every kth image.
        '''
        if not os.path.exists(video_path):
            raise IOError("Video not exist: " + video_path)
        assert isinstance(sample_interval, int) and sample_interval >= 1
        self.cnt_imgs = 0
        self._is_stoped = False
        self._video = cv2.VideoCapture(video_path)
        ret, image = self._video.read()
        self._next_image = image
        self._sample_interval = sample_interval
        self._fps = self.get_fps()
        if not self._fps >= 0.0001:
            warnings.warn("Invalid fps of video: {}".format(video_path))
        self._num_frames = int(self._video.get(cv2.CAP_PROP_FRAME_COUNT))
        self._frame_width = int(self._video.get(cv2.CAP_PROP_FRAME_WIDTH))
        self._frame_height = int(self._video.get(cv2.CAP_PROP_FRAME_HEIGHT))



    def load_new_video(self, video_path):
        if not os.path.exists(video_path):
            raise IOError("Video not exist: " + video_path)
        self.cnt_imgs = 0
        self._is_stoped = False
        self._video = cv2.VideoCapture(video_path)
        ret, image = self._video.read()
        self._next_image = image
        self._fps = self.get_fps()
        if not self._fps >= 0.0001:
            warnings.warn("Invalid fps of video: {}".format(video_path))
        self._num_frames = int(self._video.get(cv2.CAP_PROP_FRAME_COUNT))
        self._frame_width = int(self._video.get(cv2.CAP_PROP_FRAME_WIDTH))
        self._frame_height = int(self._video.get(cv2.CAP_PROP_FRAME_HEIGHT))

    def has_image(self):
        return self._next_image is not None

    def get_curr_video_time(self):
        return 1.0 / self._fps * self.cnt_imgs

    def read_image(self):
        image = self._next_image
        for i in range(self._sample_interval):
            if self._video.isOpened():
                ret, frame = self._video.read()
                self._next_image = frame
            else:
                self._next_image = None
                break
        self.cnt_imgs += 1
        return ret, image

    def stop(self):
        self._video.release()
        self._is_stoped = True

    def __del__(self):
        if not self._is_stoped:
            self.stop()

    def get_fps(self):

        # Find OpenCV version
        (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')

        # With webcam get(CV_CAP_PROP_FPS) does not work.
        # Let's see for ourselves.

        # Get video properties
        if int(major_ver) < 3:
            fps = self._video.get(cv2.cv.CV_CAP_PROP_FPS)
        else:
            fps = self._video.get(cv2.CAP_PROP_FPS)
        return fps


class SlidingWindowFromVideo(ReadFromVideo):
    """Subclass for generating a dynamic sliding window over video or live stream"""
    def __init__(self, video_path, window_size, sample_interval=1, window_sample_interval=1):
        super(SlidingWindowFromVideo, self).__init__(video_path, sample_interval)

        self.window_size = window_size
        self.window_sample_interval = window_sample_interval # stride
        self._window_frames = []

    def read_window(self):
        """Returns image stack"""
        # init window 
        if len(self._window_frames) == 0:
            for i in range(self.window_size):
                ret, img = self.read_image()
                self._window_frames.append(img)
        # remove last frames and update next frames
        else:
            del self._window_frames[0:self.window_sample_interval]
            for i in range(self.window_sample_interval):
                ret, img = self.read_image()
                self._window_frames.append(img)
        return self._window_frames