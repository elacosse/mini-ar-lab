# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import os
import pandas as pd
from tqdm import tqdm
import json
import mmcv

from src.data.utils import load_pose
from src.data.utils import make_pose_dict

@click.command()
@click.argument('data_id', type=str)
@click.argument('cv_id', type=str)
@click.argument('data_type', type=str)
@click.argument('pose_config_name', type=str)
@click.argument('selected_labels', required=False, type=click.Path())
def main(cv_id, data_id, data_type, pose_config_name, selected_labels=None):
    """Create annotation files to feed into model training from either precomputed skeletal action
    recognition or information about videos.
    It handles skeletons and RGB. Skeletons have to be pre-computed.

    Args:
        cv_id (str): train, valid, or test
        data_id (str): IsoGD, ConGD, SLR
        data_type (str): skeleton, rgb, or depth
        pose_config_name (str): yml with pose configuration
        selected_labels (FilePath, optional): JSON with selected labels. Defaults to None.
    """
    ##############################################################################
    # SLR dataset
    ##############################################################################
    if data_id == 'SLR':
        if cv_id == 'train':
            fp = os.path.join(DATA_ROOT, 'raw/SLR/', 
                'train_labels.csv')
        elif cv_id == 'valid':
            fp = os.path.join(DATA_ROOT, 'raw/SLR/',
                'valid_labels.csv')
        elif cv_id == 'test':
            fp = os.path.join(DATA_ROOT, 'raw/SLR/',
                'test_labels.csv')
        video_list_df = pd.read_csv(fp, sep=',', names=['sample_id', 'label'])

        if data_type == 'skeleton':
            data_list = []
            output_path_temp = os.path.join(DATA_ROOT,
                'processed/SLR/annotations_{class_num}/{cv_id}_skeleton.pkl')
            class_num = video_list_df['label'].unique().size
            output_path = output_path_temp.format(class_num=class_num, cv_id=cv_id)
            os.makedirs(os.path.dirname(output_path),exist_ok=True)

            for index, row in tqdm(video_list_df.iterrows(), total=video_list_df.shape[0]):
                try:
                    frame_dir = os.path.join(DATA_ROOT, 'raw/SLR', cv_id, row['sample_id'])
                    label = row['label'] # 
                    video_root_dir= os.path.join(DATA_ROOT, 'processed/SLR', cv_id,
                        row['sample_id'])
                    kpts, bboxes, size, fps = load_pose(video_root_dir, pose_config_name)
                    results = make_pose_dict(kpts, bboxes, size, fps, frame_dir, label)
                    data_list.append(results)
                except Exception as e:
                    logging.error(e)
            mmcv.dump(data_list, file=output_path)

        elif data_type == 'rgb':
            rgb_txt_output_path_temp = os.path.join(DATA_ROOT,
                'processed/SLR/annotations_{class_num}/{cv_id}_rgb.txt')
            class_num = video_list_df['label'].unique().size
            output_path = rgb_txt_output_path_temp.format(class_num=class_num, cv_id=cv_id)
            os.makedirs(os.path.dirname(output_path), exist_ok=True)
            with open(output_path, 'w') as f:
                for index, row in tqdm(video_list_df.iterrows(), total=video_list_df.shape[0]):
                    try:
                        frame_file = os.path.join(cv_id, row['sample_id']+'_color.mp4')
                        if os.path.isfile(os.path.join(DATA_ROOT, 'raw/SLR', frame_file)) and (row['sample_id'] != 'signer10_sample303'):
                            label = str(row['label'])
                            line = frame_file + ' ' + str(label) + '\n'
                            f.write(line)
                    except Exception as e:
                        logging.error(e)
    ##############################################################################
    # ConGD - ChaLearn dataset - 'ConGD'
    ##############################################################################
    if data_id == 'ConGD':
        # make continous 
        base_dir = data_id
        if cv_id == 'train':
            fp = os.path.join(DATA_ROOT, 'raw/ConGD/ConGD_labels', 
                'train.txt')
            base_dir = os.path.join(base_dir, 'ConGD_phase_1')
            phase_id = 1
        elif cv_id == 'valid':
            fp = os.path.join(DATA_ROOT, 'raw/ConGD/ConGD_labels', 
                'valid.txt')
            base_dir = os.path.join(base_dir, 'ConGD_phase_1')
            phase_id = 1
        elif cv_id == 'test':
            fp = os.path.join(DATA_ROOT, 'raw/ConGD/ConGD_labels', 
                'test.txt')
            base_dir = os.path.join(base_dir, 'ConGD_phase_2')
            phase_id = 2

        # read each row and parse into 
        video_list_df = pd.DataFrame(columns=['RGB', 'Depth', 'label', 'start', 'stop'])
        with open(fp) as f:
            lines = f.read().splitlines()
            for i, line in enumerate(lines):
                l = line.split(' ')
                video_id = l.pop(0)
                for frames in [e.split(':') for e in l]:
                    label = frames[-1]
                    start = frames[0].split(',')[0]
                    stop = frames[0].split(',')[1]
                    d = [os.path.join(cv_id, video_id.split('/')[0], \
                                    'M_'+video_id.split('/')[1]+'.avi'),\
                         os.path.join(cv_id, video_id.split('/')[0], \
                                    'K_'+video_id.split('/')[1]+'.avi'),\
                         int(label)-1, \
                         int(start)-1,
                         int(stop)-1]
                    video_list_df.loc[i] = d 

        # take label subset if necessary
        if selected_labels is not None:
            selected_labels_json_path = os.path.join(DATA_ROOT, 'raw/ConGD', selected_labels)
            with open(selected_labels_json_path) as f:
                selected_labels = json.load(f)
                for i,k in enumerate(selected_labels.keys()):
                    selected_labels[k] = i
                for index, row in tqdm(video_list_df.iterrows(), total=video_list_df.shape[0]):
                    if str(row['label']) in selected_labels.keys():
                        video_list_df.loc[index,'label'] = \
                            int(selected_labels[str(row['label'])])+1 # add one because they start from 0
                    else:
                        video_list_df = video_list_df.drop(index)
        print(video_list_df['label'].unique())

    
        if data_type == 'rgb':
            rgb_txt_output_path_temp = os.path.join(DATA_ROOT, 
                'processed/ConGD/annotations_{class_num}/{cv_id}_rgb.txt')
            class_num = video_list_df['label'].unique().size
            output_path = rgb_txt_output_path_temp.format(class_num=class_num, cv_id=cv_id)
            os.makedirs(os.path.dirname(output_path), exist_ok=True)
            with open(output_path, 'w') as f:
                for index, row in tqdm(video_list_df.iterrows(), total=video_list_df.shape[0]):
                    frame_dir = os.path.join(base_dir, row['RGB'])
                    label = row['label']-1
                    line = frame_dir + ' ' + str(label) + '\n'
                    f.write(line)

    ##############################################################################
    # IsoGD - ChaLearn dataset - 'IsoGD'
    ##############################################################################
    if data_id == 'IsoGD':
        base_dir = data_id
        if cv_id == 'train':
            fp = os.path.join(DATA_ROOT, 'raw/IsoGD/IsoGD_labels', 
                'train.txt')
            base_dir = os.path.join(base_dir, 'IsoGD_phase_1')
            phase_id = 1
        elif cv_id == 'valid':
            fp = os.path.join(DATA_ROOT, 'raw/IsoGD/IsoGD_labels',
                'valid.txt')
            base_dir = os.path.join(base_dir, 'IsoGD_phase_1')
            phase_id = 1
        elif cv_id == 'test':
            fp = os.path.join(DATA_ROOT, 'raw/IsoGD/IsoGD_labels',
                'test.txt')
            base_dir = os.path.join(base_dir, 'IsoGD_phase_2')
            phase_id = 2
        video_list_df = pd.read_csv(fp, sep=' ', names=['RGB', 'Depth', 'label'])
        
        # SELECTED LABELS
        if selected_labels is not None:
            selected_labels_json_path = os.path.join(DATA_ROOT, 'raw/IsoGD', selected_labels)
            with open(selected_labels_json_path) as f:
                selected_labels = json.load(f)
                label_dict = {int(k) : int(v) for k, v in selected_labels.items()} 
        else:
            label_dict = dict()
            for i,l in enumerate(video_list_df['label'].unique()):
                label_dict[l] = l-1 # labels start at 1 in original
        # replace labels with integers
        video_list_df['label'] = video_list_df['label'].map(label_dict)
        video_list_df = video_list_df.dropna()
        video_list_df['label'] = video_list_df['label'].astype(int)
        logging.info(video_list_df.groupby('label').count())
        if data_type == 'skeleton':
            data_list = []
            output_path_temp = os.path.join(DATA_ROOT, 
                'processed/IsoGD/annotations_{class_num}/{cv_id}_skeleton.pkl')
            class_num = video_list_df['label'].unique().size
            output_path = output_path_temp.format(class_num=class_num, cv_id=cv_id)
            os.makedirs(os.path.dirname(output_path),exist_ok=True)
            for index, row in tqdm(video_list_df.iterrows(), total=video_list_df.shape[0]):
                frame_dir = os.path.join(base_dir, row['RGB'])
                label = row['label']
                video_root_dir= os.path.join(DATA_ROOT, 'processed/IsoGD',
                    'IsoGD_phase_'+str(phase_id), row['RGB'][:-4])
                kpts, bboxes, size, fps = load_pose(video_root_dir, pose_config_name)
                results = make_pose_dict(kpts, bboxes, size, fps, frame_dir, label)
                data_list.append(results)
            mmcv.dump(data_list, file=output_path)

        elif data_type == 'rgb':
            rgb_txt_output_path_temp = os.path.join(DATA_ROOT, 
                'processed/IsoGD/annotations_{class_num}/{cv_id}_rgb.txt')
            class_num = video_list_df['label'].unique().size
            output_path = rgb_txt_output_path_temp.format(class_num=class_num, cv_id=cv_id)
            os.makedirs(os.path.dirname(output_path), exist_ok=True)
            with open(output_path, 'w') as f:
                for index, row in tqdm(video_list_df.iterrows(), total=video_list_df.shape[0]):
                    frame_dir = os.path.join(base_dir, row['RGB'])
                    label = row['label']
                    line = frame_dir + ' ' + str(label) + '\n'
                    f.write(line)

if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]
    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    DATA_ROOT = os.environ.get("DATA_ROOT")
    main()
