# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import os
import json
import numpy as np
from numpy.core.fromnumeric import size
import pandas as pd
from tqdm import tqdm

import mmcv

import cv2
import src.data.utils as data_utils
from src.features.utils import CONDENSED_KPTS
from src.features.utils import normalize_wbcoco_skeleton


def print_stats_of_anno(annos, new_annos, num_classes):
    anno_labels = [anno['label'] for anno in annos]
    new_anno_labels = [anno['label'] for anno in new_annos]
    for i in range(num_classes):
        print(i, anno_labels.count(i), new_anno_labels.count(i))

def check_solve(kp0, kps0, kp1, kps1, kpt_threshold=None, dist_threshold=None):
    CONDENSED_KPTS = [0,5,6,7,8,9,10] # neck, l_shoulder, r_shoulder, l_elbow, r_elbow, l_wrist, r_wrist
    # [91,95,96,99,100,103,104,107,108,111], # condensed right hand
    # [112,116,117,120,121,124,125,128,129,132]), # condensed  left hand 
    if kpt_threshold is not None:
        if np.sum(kps0[0,0,CONDENSED_KPTS] < kpt_threshold) != 0:
            return False
        if np.sum(kps1[0,0,CONDENSED_KPTS] < kpt_threshold) != 0:
            return False   
    nkp0 = normalize_wbcoco_skeleton(kp0[0])
    nkp1 = normalize_wbcoco_skeleton(kp1[0])
    dist = np.linalg.norm(nkp0[0,:,:] - nkp1[0,:,:])
    if np.isnan(dist):
       return False
    elif dist_threshold is not None:
        if dist < dist_threshold:
            return False
    return dist


@click.command()
@click.argument('cv_id', type=str)
@click.argument('data_id', type=str)
@click.argument('num_classes', type=int)
@click.argument('kpt_threshold', type=float)
def main(cv_id, data_id, num_classes, kpt_threshold):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making pose data set from raw video feed')

    anno_temp = os.path.join(project_dir, 'data/processed/', 
        data_id, 'annotations_{num_classes}', '{cv_id}_skeleton.pkl')
    anno_save_temp = os.path.join(project_dir, 'data/processed/', 
        data_id, 'annotations_{num_classes}', '{cv_id}_skeleton_static.pkl')
    try:
        annos = mmcv.load(anno_temp.format(num_classes=num_classes, cv_id=cv_id))
    except Exception as e:
        logging.error(e) 
    rejected = 0
    new_annos = []
    for anno in tqdm(annos, total=len(annos)):
        new_anno = dict()
        middle_frame_index = int(anno['total_frames']/2)
        kp0 = anno['keypoint'][:,0:1]
        kps0 = anno['keypoint_score'][:,0:1]
        # Check score in critical keypoints
        frames = 3
        try:
            for i in range(-frames, frames):
                kp = anno['keypoint'][:,middle_frame_index-i:middle_frame_index-i+1]
                kp_score = anno['keypoint_score'][:,middle_frame_index-i:middle_frame_index-i+1]
                dist = check_solve(kp0,kps0,kp,kp_score,kpt_threshold)
                if dist:
                    new_anno['keypoint'] = kp
                    new_anno['keypoint_score'] = kp_score
                    new_anno['frame_dir'] = anno['frame_dir']
                    new_anno['label'] = anno['label']
                    new_anno['img_shape'] = anno['img_shape']
                    new_anno['total_frames'] = 1
                    new_anno['fps'] = anno['fps']
                    new_anno['dist'] = dist
                    new_annos.append(new_anno)
            if dist is False:
                rejected += 1
                    # CONDENSED_KPTS = [0,5,6,7,8,9,10]
                    # print(anno['label'], dist, anno['keypoint_score'][0,0,CONDENSED_KPTS])
                    # print(anno['keypoint_score'][0,0,CONDENSED_KPTS] < 0.1)
        except Exception as e:
            logging.error(e)
    
            
    try:
        mmcv.dump(new_annos, anno_save_temp.format(num_classes=num_classes, cv_id=cv_id), file_format='pkl')
        logging.info('completed and saved')
        print_stats_of_anno(annos, new_annos, num_classes)
    except Exception as e:
        logging.error(e) 
    logging.info('Rejected samples: '+ str(rejected))
if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]
    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    main()
