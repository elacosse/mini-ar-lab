# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import os
import json
import numpy as np
from numpy.core.fromnumeric import size
import pandas as pd
from tqdm import tqdm

import mmcv

import cv2
import src.data.utils as data_utils
import src.features.Pose as Pose
from src.data.utils import create_pose_data

@click.command()
@click.argument('cv_id', type=str)
@click.argument('pose_config', type=str)
@click.argument('save_out_video', type=bool)
def main(cv_id, pose_config, save_out_video):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making pose data set from raw video feed')

    if cv_id == 'train':
        fp = os.path.join(DATA_ROOT, 'raw/IsoGD/IsoGD_labels', 
            'train.txt')
    elif cv_id == 'valid':
        fp = os.path.join(DATA_ROOT, 'raw/IsoGD/IsoGD_labels',
            'valid.txt')
    elif cv_id == 'test':
        fp = os.path.join(DATA_ROOT, 'raw/IsoGD/IsoGD_labels',
            'test.txt')
    if cv_id == 'train' or cv_id == 'valid':
        phase_id = 1
    elif cv_id == 'test':
        phase_id = 2

    video_list_df = pd.read_csv(fp, sep=' ', names=['RGB', 'Depth', 'GestureID'])

    ###### init pose model object #####################
    pose = Pose.Pose(pose_config)
    pose_config_name = pose.pose_config_name 
    ###################################################
    input_path = os.path.join(DATA_ROOT, 'raw/IsoGD')
    output_path = os.path.join(DATA_ROOT, 'processed/IsoGD')
    for index, row in tqdm(video_list_df.iterrows(), total=video_list_df.shape[0]):
        # input
        video_path = os.path.join(input_path, 'IsoGD_phase_'+str(phase_id), row['RGB'])
        # output
        out_video_root = os.path.join(output_path,'IsoGD_phase_'+str(phase_id), row['RGB'][:-4])

        if not os.path.isfile(os.path.join(out_video_root, 'keypoints_'+ pose_config_name + '.npy')):
            create_pose_data(out_video_root, pose_config_name, video_path,\
                output_path, pose, save_out_video=save_out_video)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]
    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    DATA_ROOT = os.environ.get("DATA_ROOT")
    main()
