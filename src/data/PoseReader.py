import os
import warnings
import numpy as np
import cv2
import time
import glob
import threading
import queue
import multiprocessing

import os
from dotenv import find_dotenv, load_dotenv

from pathlib import Path
from timeit import default_timer as timer
class ReadFromFolder(object):
    def __init__(self, config_filepath, folder_path=None, sample_interval=1):
        ''' A reader class for reading pose outputs belonging in folder
        Arguments:
            folder_path
            config_file
            sample_interval {int}: sample every kth image.
        '''  
        load_dotenv(config_filepath, override=True)
        self.pose_config_name = os.getenv("POSECONFIGNAME")
        self.body_model_id = os.getenv("BODYMODELID")

        if self.body_model_id == 'wbcoco':
            self.body_kp_dim = 133
        else:
            self.body_kp_dim = 0 

        self._sample_interval = sample_interval
        self.keypoints = None
        self.heatmaps = None
        self.bboxes = None
        self._video = None
        self._is_stopped = True
        
        self.cnt_imgs = 0
        self.cnt_kp = 0

        self._next_image = None
        self._next_kp = None

        if folder_path is not None:
            ######## POSE KP ###################
            try:
                fp = os.path.join(folder_path, 'keypoints_'+self.pose_config_name+'.npy')
                self.keypoints = np.load(fp)
                kp = self.keypoints[0]
                self._next_kp = kp
            except Exception as e:
                warnings.warn(e)
            ######## HEATMAPS ##################
            try:
                fp =os.path.join(folder_path, 'heatmap_'+self.pose_config_name+'.npy')
                self.heatmaps = np.load(fp)
                assert self.heatmaps.shape[0] == self.keypoints.shape[0]
            except Exception as e:
                warnings.warn("Heatmaps don't exist")
            ######## BBOX ##################
            try:
                fp = os.path.join(folder_path, 'bbox_'+self.pose_config_name+'.npy')
                self.bboxes = np.load(fp)
                assert self.bboxes.shape[0] == self.keypoints.shape[0]
            except Exception as e:
                warnings.warn("Bboxes don't exist")
            ######## VIDEO #####################
            try:
                assert isinstance(sample_interval, int) and sample_interval >= 1
                video_path = os.path.join(folder_path, 'vis_'+self.pose_config_name+'.mp4')
                if not os.path.exists(video_path):
                    warnings.warn("Video don't exist: " + video_path)
                else:
                    self.cnt_imgs = 0
                    self._is_stopped = False
                    self._video = cv2.VideoCapture(video_path)
                    ret, image = self._video.read()
                    self._next_image = image
                    self._fps = self.get_fps()
                    if not self._fps >= 0.0001:
                        warnings.warn("Invalid fps of video: {}".format(video_path))
            except Exception as e:
                warnings.warn("Video doesn't exist")
            
    def load_annotation(self, anno):
        """init preloaded keypoints from annotation structure."""
        self.keypoints = np.concatenate([
            anno['keypoint'][0],
            anno['keypoint_score'][0][:,:,np.newaxis]
            ], axis=-1)

        kp = self.keypoints[0]
        self._next_kp = kp

    def has_image(self):
        return self._next_image is not None

    def has_kp(self):
        return self._next_kp is not None

    def get_curr_video_time(self):
        return 1.0 / self._fps * self.cnt_imgs

    def read_image(self):
        image = np.nan
        if self._next_image is not None:
            image = self._next_image
            for i in range(self._sample_interval):
                if self._video.isOpened():
                    ret, frame = self._video.read()
                    self._next_image = frame
                else:
                    self._next_image = None
                    break
        self.cnt_imgs += 1
        return image

    def read_keypoint(self):
        kp = np.nan
        if self._next_kp is not None:
            kp = self._next_kp
            for i in range(self._sample_interval):
                if self.cnt_kp >= self.keypoints.shape[0]:
                    self._next_kp = None
                    break
                else:
                    kp = self.keypoints[self.cnt_kp]
                    self._next_kp = kp
            self.cnt_kp += self._sample_interval
        # print(kp[0,0], self.cnt_kp)
        return kp


    def stop(self):
        self._video.release()
        self._is_stopped = True

    def __del__(self):
        if not self._is_stopped:
            self.stop()

    def get_fps(self):

        # Find OpenCV version
        (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')

        # With webcam get(CV_CAP_PROP_FPS) does not work.
        # Let's see for ourselves.

        # Get video properties
        if int(major_ver) < 3:
            fps = self._video.get(cv2.cv.CV_CAP_PROP_FPS)
        else:
            fps = self._video.get(cv2.CAP_PROP_FPS)
        return fps


class SlidingWindowFromVideo(ReadFromFolder):
    """Subclass for generating a dynamic sliding window over video or live stream
    Args:
        window_size : size of window (int)
        sample_interval : what frame to sample (int, default = 1, i.e., every frame)
        window_sample_interval : how many frames before new window (int, default = 1)
    """
    def __init__(self, config_filepath, window_size, folder_path=None,\
            sample_interval=1, window_sample_interval=1):

        super(SlidingWindowFromVideo, self).__init__(config_filepath,\
                folder_path, sample_interval)
        
        self.window_size = window_size
        self.window_sample_interval = window_sample_interval
        self._skeleton_window_frames = []
        # initialize skeleton array
        self._skeleton_arr = np.empty((self.window_size,self.body_kp_dim,3))
        self.track_indx = 0 # always select first index NO TRACKING IMPLEMENTED

    def read_window(self):
        """Returns image stack"""
        # init window 
        if self.keypoints is not None:
            if self.cnt_kp == 0:
                for i in range(self.window_size):
                    self._skeleton_arr[i] = \
                        self.read_keypoint()
            else:
                # update np array
                self._skeleton_arr = np.roll(self._skeleton_arr, 
                    -self.window_sample_interval, axis=0
                )
                for i in range(self.window_sample_interval):
                    self._skeleton_arr[-self.window_sample_interval+i] = \
                        self.read_keypoint()

        # video
        if self._video is not None:
            if len(self._skeleton_window_frames) == 0:
                for i in range(self.window_size):
                    img = self.read_image()
                    self._skeleton_window_frames.append(img)
            # remove last frames and update next frames
            else:
                del self._skeleton_window_frames[0:self.window_sample_interval]
                for i in range(self.window_sample_interval):
                    img = self.read_image()
                    self._skeleton_window_frames.append(img)

        return self._skeleton_window_frames, self._skeleton_arr