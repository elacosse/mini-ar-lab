# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import os
import json
import numpy as np
from numpy.core.fromnumeric import size
import pandas as pd
from tqdm import tqdm

import mmcv

import cv2
import src.data.utils as data_utils
import src.features.Pose as Pose

@click.command()
@click.argument('cv_id', type=str)
@click.argument('pose_config', type=str)
@click.argument('save_out_video', type=bool)
@click.argument('output_filepath', type=click.Path())
def main(cv_id, pose_config, save_out_video, output_filepath):
    """Runs data processing scripts to turn raw data from (../raw) into
       skeleton data ready to be analyzed (e.g., ../processed).

    Args:
        cv_id (str): train, valid, or test
        pose_config ([type]): [description]
        save_out_video ([type]): [description]
        output_filepath ([type]): [description]
    """
    logger = logging.getLogger(__name__)
    logger.info('making pose data set from raw video feed')

    if cv_id == 'train':
        fp = os.path.join(DATA_ROOT, 'raw/ConGD/ConGD_labels', 
            'train.txt')
    elif cv_id == 'valid':
        fp = os.path.join(DATA_ROOT, 'raw/ConGD/ConGD_labels',
            'valid.txt')
    elif cv_id == 'test':
        fp = os.path.join(DATA_ROOT, 'raw/ConGD/ConGD_labels',
            'test.txt')
    if cv_id == 'train' or cv_id == 'valid':
        phase_id = 1
    elif cv_id == 'test':
        phase_id = 2

    # read each row and parse into 
    video_list_df = pd.DataFrame(columns=['RGB', 'Depth', 'label', 'start', 'stop'])
    with open(fp) as f:
        lines = f.read().splitlines()
        for i, line in enumerate(lines):
            l = line.split(' ')
            video_id = l.pop(0)
            for frames in [e.split(':') for e in l]:
                label = frames[-1]
                start = frames[0].split(',')[0]
                stop = frames[0].split(',')[1]
                d = [os.path.join(cv_id, video_id.split('/')[0], \
                                'M_'+video_id.split('/')[1]+'.avi'),\
                        os.path.join(cv_id, video_id.split('/')[0], \
                                'K_'+video_id.split('/')[1]+'.avi'),\
                        int(label)-1, \
                        int(start)-1,
                        int(stop)-1]
                video_list_df.loc[i] = d 

    ###### init pose model object #####################
    pose = Pose.Pose(pose_config)
    pose_config_name = pose.pose_config_name # os.path.basename(pose_config)[:-4]
    ###################################################
    
    for index, row in tqdm(video_list_df.iterrows(), total=video_list_df.shape[0]):
        video_path = os.path.join(DATA_ROOT, 'raw/ConGD', 
            'ConGD_phase_'+str(phase_id), row['RGB'])
        out_video_root = os.path.join(DATA_ROOT, output_filepath,
            'ConGD_phase_'+str(phase_id), row['RGB'][:-4])

        frame_dir = row['RGB']
        label = int(row['GestureID'])-1 # start at 0 

        logging.info('processing -> ' + video_path)
        cap = cv2.VideoCapture(video_path)
        assert cap.isOpened(), f'Faild to load video file {video_path}'
        fps = cap.get(cv2.CAP_PROP_FPS)
        size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
                int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        original_shape = size

        # create output directory
        if output_filepath == '':
            save_out_video = False
        else:
            os.makedirs(out_video_root, exist_ok=True)

        if save_out_video:
            fourcc = cv2.VideoWriter_fourcc(*'mp4v')
            videoWriter = cv2.VideoWriter(
                os.path.join(out_video_root,
                    f'vis_{pose_config_name}.mp4'),\
                                    fourcc, fps, size)

        # store results as numpy array
        np_kpts_list, np_bbox_list, np_heatmap_list = [], [], []
        while (cap.isOpened()):
            flag, img = cap.read()
            if not flag:
                break
            # estimate skeleton of image
            pose_results, returned_outputs = pose.estimate_skeleton_of_img(img)
            # convert from dictionary to np arrays
            if len(pose_results) != 0:
                kpts = np.expand_dims(pose_results[0]['keypoints'], axis=0)
                np_kpts_list.append(kpts)
                bbox = np.expand_dims(pose_results[0]['bbox'], axis=0)
                np_bbox_list.append(bbox)
                if 'heatmap' in returned_outputs[0].keys():
                    heatmap = returned_outputs[0]['heatmap'][0:1]
                    np_heatmap_list.append(heatmap)
            else:
                # fill with nan so frame number consistent
                kpts = np.empty((1,pose.kp_dim,3)) # 1 x keypoints x x,y,score
                kpts[:] = np.nan
                np_kpts_list.append(kpts)
                bbox = np.empty((1,5))
                bbox[:] = np.nan
                np_bbox_list.append(bbox)
                heatmap = np.empty((1, pose.kp_dim, pose.heatmap_size[1], pose.heatmap_size[0]))
                np_heatmap_list.append(heatmap)
            
            vis_img = pose.vis_pose_result_of_img(img, pose_results)
            if save_out_video:
                videoWriter.write(vis_img)
        
        kpts = np.vstack(np_kpts_list)
        bboxes = np.vstack(np_bbox_list)
        if pose.return_heatmap:
            heatmaps = np.vstack(np_heatmap_list)
        assert bboxes.shape[0] == kpts.shape[0]

        # write results to disk
        try:
            # numpy arrays
            np_filepath = os.path.join(out_video_root,
                f'keypoints_{pose_config_name}.npy')
            np.save(np_filepath, kpts)
            np_filepath = os.path.join(out_video_root,
                f'bbox_{pose_config_name}.npy')
            np.save(np_filepath, bboxes)
            if pose.return_heatmap:
                np_filepath = os.path.join(out_video_root,
                    f'heatmap_{pose_config_name}.npy')
                np.save(np_filepath, heatmaps)
            if save_out_video:
                videoWriter.release()
            logging.info('completed and saved')
        except Exception as e:
            logging.error(e)

if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]
    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    DATA_ROOT = os.environ.get("DATA_ROOT")
    main()
