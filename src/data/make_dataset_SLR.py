# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import os
import json
import numpy as np
from numpy.core.fromnumeric import size
import pandas as pd
from tqdm import tqdm

import mmcv

import cv2
import src.data.utils as data_utils
import src.features.Pose as Pose
from src.data.utils import create_pose_data

@click.command()
@click.argument('cv_id', type=str)
@click.argument('pose_config', type=str)
@click.argument('save_out_video', type=bool)
def main(cv_id, pose_config, save_out_video):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making pose data set from raw video feed')
    # Get appropriate labels from SLR csv
    if cv_id == 'train':
        fp = os.path.join(DATA_ROOT, 'raw/SLR/', 
            'train_labels.csv')
    elif cv_id == 'valid':
        fp = os.path.join(DATA_ROOT, 'raw/SLR/',
            'valid_labels.csv')
    elif cv_id == 'test':
        fp = os.path.join(DATA_ROOT, 'raw/SLR/',
            'test_labels.csv')
    video_list_df = pd.read_csv(fp, sep=',', names=['sample_id', 'label'])
    ###### init pose model object #####################
    pose = Pose.Pose(pose_config)
    pose_config_name = pose.pose_config_name 
    ###################################################
    input_path = os.path.join(DATA_ROOT, 'raw/SLR')
    output_path = os.path.join(DATA_ROOT, 'processed/SLR')
    for index, row in tqdm(video_list_df.iterrows(), total=video_list_df.shape[0]):
        # input
        video_path = os.path.join(input_path, cv_id, row['sample_id']+'_color.mp4')
        # output
        out_video_root = os.path.join(output_path, cv_id, row['sample_id'])
        # frame directory
        # frame_dir = row['sample_id']
        # label = int(row['label']) # labels start already at 0
        if not os.path.isfile(os.path.join(out_video_root, 'keypoints_'+ pose_config_name + '.npy')):
            create_pose_data(
                out_video_root, pose_config_name, video_path,
                output_path, pose, save_out_video=save_out_video
            )

if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]
    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    # data directory where data/raw and data/processed
    DATA_ROOT = os.environ.get("DATA_ROOT")  
    main()
