import os
import numpy as np
import cv2
import logging

def load_pose(out_video_root, pose_config_name):
    """Loads pose model output 

    Args:
        out_video_root (Folder Path): Folder with output of pose model
        pose_config_name (str): configuration name of pose model

    Returns:
        [list]: keypoints, bounding-box, size of image, and fps
    """
    np_filepath = os.path.join(out_video_root,
        f'keypoints_{pose_config_name}.npy')
    kpts = np.load(np_filepath, )
    np_filepath = os.path.join(out_video_root,
        f'bbox_{pose_config_name}.npy')
    bboxes = np.load(np_filepath)
    video_path = os.path.join(out_video_root,
                    f'vis_{pose_config_name}.mp4')
    cap = cv2.VideoCapture(video_path)
    assert cap.isOpened(), f'Failed to load video file {video_path}'
    fps = cap.get(cv2.CAP_PROP_FPS)
    size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
            int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
    return kpts, bboxes, size, fps

def make_pose_dict(kpts, bboxes, size, fps, frame_dir, label):
    """Creates a pose annotation entry.

    Args:
        kpts (numpy arr): [description]
        bboxes ([type]): [description]
        size ([type]): [description]
        fps ([type]): [description]
        frame_dir ([type]): [description]
        label ([type]): [description]

    Returns:
        [type]: [description]
    """
    img_shape = size
    original_shape = size
    total_frames = kpts.shape[0]
    results = {
        'frame_dir': frame_dir, 
        'label': label, 
        'img_shape': img_shape, 
        'original_shape': original_shape, 
        'total_frames': total_frames, 
        'fps': fps,
        'keypoint': np.nan_to_num(np.expand_dims(kpts[:,:,0:2], axis=0)), 
        'keypoint_score': np.nan_to_num(np.expand_dims(kpts[:,:,2], axis=0)),
        'bbox': np.expand_dims(bboxes[:,0:4], axis=0),
        'bbox_score': np.expand_dims(bboxes[:,4], axis=0),
    }
    return results

def create_pose_data(out_video_root, pose_config_name, video_path,\
        output_filepath, pose, save_out_video=True):
    np_filepath = os.path.join(out_video_root,
                    f'keypoints_{pose_config_name}.npy')
                
    
    cap = cv2.VideoCapture(video_path)
    assert cap.isOpened(), f'Failed to load video file {video_path}'
    fps = cap.get(cv2.CAP_PROP_FPS)
    size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
            int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
    original_shape = size

    # create output directory
    if output_filepath == '':
        save_out_video = False
    else:
        os.makedirs(out_video_root, exist_ok=True)

    if save_out_video:
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        videoWriter = cv2.VideoWriter(
            os.path.join(out_video_root,
                f'vis_{pose_config_name}.mp4'),\
                                fourcc, fps, size)

    # store results as numpy array
    np_kpts_list, np_bbox_list, np_heatmap_list = [], [], []
    while (cap.isOpened()):
        flag, img = cap.read()
        if not flag:
            break
        # estimate skeleton of image
        pose_results, returned_outputs = pose.estimate_skeleton_of_img(img)
        # convert from dictionary to np arrays
        if len(pose_results) != 0:
            kpts = np.expand_dims(pose_results[0]['keypoints'], axis=0)
            np_kpts_list.append(kpts)
            bbox = np.expand_dims(pose_results[0]['bbox'], axis=0)
            np_bbox_list.append(bbox)
            if 'heatmap' in returned_outputs[0].keys():
                heatmap = returned_outputs[0]['heatmap'][0:1]
                np_heatmap_list.append(heatmap)
        else:
            # fill with nan so frame number consistent
            kpts = np.empty((1,pose.kp_dim,3)) # 1 x keypoints x x,y,score
            kpts[:] = np.nan
            np_kpts_list.append(kpts)
            bbox = np.empty((1,5))
            bbox[:] = np.nan
            np_bbox_list.append(bbox)
            heatmap = np.empty((1, pose.kp_dim, pose.heatmap_size[1], pose.heatmap_size[0]))
            np_heatmap_list.append(heatmap)
        
        vis_img = pose.vis_pose_result_of_img(img, pose_results)
        if save_out_video:
            videoWriter.write(vis_img)

    kpts = np.vstack(np_kpts_list)
    bboxes = np.vstack(np_bbox_list)
    if pose.return_heatmap:
        heatmaps = np.vstack(np_heatmap_list)
    assert bboxes.shape[0] == kpts.shape[0]

    try:                       
        # numpy arrays
        np_filepath = os.path.join(out_video_root,
            f'keypoints_{pose_config_name}.npy')
        np.save(np_filepath, kpts)
        np_filepath = os.path.join(out_video_root,
            f'bbox_{pose_config_name}.npy')
        np.save(np_filepath, bboxes)
        if pose.return_heatmap:
            np_filepath = os.path.join(out_video_root,
                f'heatmap_{pose_config_name}.npy')
            np.save(np_filepath, heatmaps)
        if save_out_video:
            videoWriter.release()
        logging.info('completed and saved')
    except Exception as e:
        logging.error(e)



def get_processed_utd_mhad_filespaths(output_filepath, pose_config, a_id, s_id, t_id):
    """returns filepaths for pose video, keypoints (numpy), and bbox (numpy)
    Set up with a_id (action), s_id (subject), t_id (iteration)"""
    dataset_name = 'UTD-MHAD'
    out_root_template = os.path.join(output_filepath, dataset_name,
        'a{a_id}_s{s_id}_t{t_id}_color')
    # video results
    videoresults_filepath = os.path.join(out_root_template.format(a_id=a_id, s_id=s_id, t_id=t_id),
        f'vis_{os.path.basename(pose_config)[:-3]}.mp4')
    # numpy arrays
    keypoints_filepath = os.path.join(out_root_template.format(a_id=a_id, s_id=s_id, t_id=t_id),
        f'keypoints_{os.path.basename(pose_config)[:-3]}.npy')
    bbox_filepath = os.path.join(out_root_template.format(a_id=a_id, s_id=s_id, t_id=t_id),
        f'bbox_{os.path.basename(pose_config)[:-3]}.npy')
    return videoresults_filepath, keypoints_filepath, bbox_filepath


def process_mmdet_results(mmdet_results, cat_id=1):
    """Process mmdet results, and return a list of bboxes.

    :param mmdet_results:
    :param cat_id: category id (default: 1 for human)
    :return: a list of detected bounding boxes
    """
    if isinstance(mmdet_results, tuple):
        det_results = mmdet_results[0]
    else:
        det_results = mmdet_results

    bboxes = det_results[cat_id - 1]

    person_results = []
    for bbox in bboxes:
        person = {}
        person['bbox'] = bbox
        person_results.append(person)

    return person_results