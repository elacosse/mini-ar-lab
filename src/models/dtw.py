from fastdtw import fastdtw
from scipy.spatial.distance import euclidean
from src.data.utils import get_processed_utd_mhad_filespaths
import src.features.utils as feature_utils
import numpy as np

def get_kpts_dict_of_actions(action_ids, output_filepath, pose_config, subject_id=1, iteration_id=1):
    """Generates a dictionary of actions for 1NN classifier.
    """
    action_dict = dict()
    for a_id in action_ids:
        videoresults_filepath, keypoints_filepath, bbox_filepath = \
            get_processed_utd_mhad_filespaths(output_filepath, pose_config, a_id,subject_id,iteration_id)
        keypoints = np.load(keypoints_filepath)
        action_dict[a_id] = keypoints
    return action_dict

def compute_dtw_dist(kpts1, kpts2, thr=0.5):
    """Normalizes, demeans, filters and computes DTW distance
    between two keypoint sequences.
    Selects keypoint dimensions above temporal std threshold"""

    # normalize, demean and filter
    kpts1 = feature_utils.normalize_wbcoco_skeleton(kpts1)
    kpts2 = feature_utils.normalize_wbcoco_skeleton(kpts2)
    kpts1 = feature_utils.apply_filter(kpts1, filter='med')
    kpts2 = feature_utils.apply_filter(kpts2, filter='med')
    # get kp dims above threshold
    kpts1_sel_dim = feature_utils.get_selected_kp_dims_above_thr(kpts1, thr)
    kpts2_sel_dim = feature_utils.get_selected_kp_dims_above_thr(kpts2, thr)
    sel_dims = kpts1_sel_dim.union(kpts2_sel_dim) # take union of two seq

    dist_sum = 0.0
    for dim in list(sel_dims):
        for coord_dim in range(0,2):
            dist, path = fastdtw(kpts1[:,dim,coord_dim], kpts2[:,dim,coord_dim], radius=30, dist=euclidean)
            dist_sum += dist
    
    norm_dist_sum = dist_sum/len(sel_dims)
    return norm_dist_sum

def classifier_1nn(keypoints, keypoints_lib, thr=0.5):
    """A Simple One-Nearest-Neighbor Classifier(1NN)
    """
    dist_lib = dict()
    for a_id in keypoints_lib.keys():
        dist_lib[a_id] = compute_dtw_dist(keypoints, keypoints_lib[a_id], thr)
    
    min_dist_action_id = min(dist_lib, key=dist_lib.get)
    return min_dist_action_id, dist_lib

