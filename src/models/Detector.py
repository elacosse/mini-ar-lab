from dotenv import find_dotenv, load_dotenv
import os
from src.features.utils import normalize_wbcoco_skeleton
import numpy as np
from dotenv import find_dotenv, load_dotenv
try:
    from mmdet.apis import inference_detector, init_detector
    has_mmdet = True
except (ImportError, ModuleNotFoundError):
    has_mmdet = False

from mmpose.apis import (inference_top_down_pose_model, init_pose_model,
                         vis_pose_result)

import src.data.utils as data_utils
from src.models.mlp import MLP
from pathlib import Path
from timeit import default_timer as timer
from mmcv import Config
from src.features.utils import CONDENSED_KPTS
import src.models.Model as Model

class Detector(object):
    """Detector for top-down recognition

    Args:
        model_config_path (str): path to model config
        model_checkpoint_path (str): path to model checkpoint
        detect_thr (float): detection threshold
        body_model_id (str): which model to use
        movement_thr (float): movement threshold
        
    """
    def __init__(self, detect_thr=0.0, model_config_path=None,
             model_checkpoint_path=None, body_model_id='wbcoco',
             movement_thr=5.0):
        
        self.detect_thr = detect_thr # some kind of metric for detect threshold

        self.kpts = None
        self.norm_kpts = None
        self.selected_keypoints = CONDENSED_KPTS

        # which inference model?
        self.body_model_id = body_model_id # whole body coco (default)
        self.movement_thr = movement_thr # arbitrary movement threshold

        if model_config_path is not None:
            self.model = Model.Model(model_config_path, model_checkpoint_path)
            self.model.score_thr = 0.8
        else:
            self.model = None

        self.last_movement_value = None
        self.last_model_output = None

    def _pose_inference(self):
        """Returns True if trained model calculates above threshold"""
        model_output = self.model.inference(self.kpts)
        self.last_model_output = model_output # convert to np arr
        if self.last_model_output[1] > self.detect_thr:
            return True
        else: return False

    def _pose_movement_inference(self):
        """Returns True if movement is below threshold"""
        self.compute_movement_value()
        if self.last_movement_value < self.movement_thr:
            return True
        else: return False

    def compute_movement_value(self, kpts=None):
        if kpts is not None:
            self.kpts = kpts
        # kpts_diff1 = np.diff(self.kpts, axis=0)[:,:,-1]
        # # mean_scores = self.kpts.mean(axis=0)[:,-1]
        # std_scores = self.kpts.std(axis=0)[:,-1]
        x_std = self.kpts.std(axis=0)[:,0]
        y_std = self.kpts.std(axis=0)[:,1]
        avg_std = x_std[CONDENSED_KPTS].mean() + y_std[CONDENSED_KPTS].mean()
        self.last_movement_value = avg_std
        # window_diff = self.kpts[-1] - self.kpts[0]
        # x_window_diff = window_diff[:,0].mean()
        # y_window_diff = window_diff[:,1].mean()
        # score_window_diff = window_diff[:,2].mean()
        #print((x_window_diff, y_window_diff), avg_std)
        return avg_std

    def solve_w_rgb(self, rgb_frames):
        """TODO"""
        return False


    def solve_with_kpts(self, kpts, option=None):
        """Updates keypoint attribute and run inference"""
        self.kpts = kpts
        if self.body_model_id == 'wbcoco':
            self.norm_kpts = normalize_wbcoco_skeleton(self.kpts)

        is_nan_detected = np.isnan(self.norm_kpts).any()
        if option is None: # all
            is_move_off = self._pose_movement_inference()
            is_start_detected = self._pose_inference()
            if is_move_off and is_start_detected and not is_nan_detected:
                return True
            else: return False
        elif option == 'motion_only':
            is_move_off = self._pose_movement_inference()
            if is_move_off and not is_nan_detected:
                return True
            else: return False
def test():
    load_dotenv(find_dotenv())
    from src.data.PoseReader import SlidingWindowFromVideo
    pose_config = 'models/configs/wbcoco_mmpose_config.yml'
    window_size = 8
    sample_interval = 2
    folder_path = ""
    swfv = SlidingWindowFromVideo(pose_config, window_size=window_size, sample_interval=sample_interval, folder_path=folder_path)
    det = Detector(detect_thr=0.5, 
            model_config='models/configs/mmaction/mlp_pose_8x2x1_150e_data-detector-mini-ar-lab.py',
            model_checkpoint="",
    )
    for i in range(100):
        frames, arr = swfv.read_window()
        if np.isnan(arr).any():
            continue
        else:
            print(i, det.solve_with_kpts(arr))