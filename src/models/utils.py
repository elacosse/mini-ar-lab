import pickle
from numpy.core.fromnumeric import mean
import torch
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
def softmax(x, dim=1):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x, axis=dim, keepdims=True))
    return e_x / e_x.sum(axis=dim, keepdims=True)

def mean_class_accuracy(scores, labels):
    """
    Compute the mean class accuracy of a model.

    Args:
        scores (np.array): The scores of the model.
        labels (list): The labels of the model.
    """
    pred = np.argmax(scores, axis=1)
    cf = confusion_matrix(labels, pred).astype(float)

    cls_cnt = cf.sum(axis=1)
    cls_hit = np.diag(cf)

    mask = cls_cnt != 0.0
    acc = cls_hit[mask]/cls_cnt[mask]
    return np.mean(acc)


def top_k_acc(score, lb_set, k=3):
    idx = np.argsort(score)[-k:]
    return len(lb_set.intersection(idx)), len(lb_set)


def top_k_hit(score, lb_set, k=3):
    idx = np.argsort(score)[-k:]
    return len(lb_set.intersection(idx)) > 0, 1


def top_k_accuracy(scores, labels, k=(1,)):
    res = []
    for kk in k:
        hits = []
        for x, y in zip(scores, labels):
            y = [y] #if isinstance(y, int) else y
            hits.append(top_k_hit(x, set(y), k=kk)[0])
        res.append(np.mean(hits))
    return res

def compute_metric_eval(y_hat, y):
    preds = np.argmax(y_hat, axis=1)
    n_classes = len(np.unique(y))
    cm = confusion_matrix(y, preds, labels=range(n_classes)).astype(float)
    class_pred = np.array(cm.sum(axis=0), dtype=int)
    acc = np.nan_to_num(np.diag(cm)/class_pred)
    f1 = f1_score(y, preds, average='weighted')
    df = pd.DataFrame({'label' : y}).value_counts()
    df.index = [x[0] for x in df.index] # annoying multiindex return from value_counts
    
    df_scores = pd.DataFrame(
        {'Accuracy': acc,
         'Prediction Support': class_pred,
         'Label': range(n_classes)}
    )
    df_scores.insert(3, 'Support', df)
    
    df_cm = pd.DataFrame(
        cm, columns=range(n_classes),
        index=range(n_classes),
    )
    
    return df_cm, acc, df_scores

def generate_model_evaluation_summary(pkl):
    model_id = pkl['model_id']
    labels = pkl['labels']
    y_hat = pkl['y_hat']
    top_1_acc, top_5_acc = top_k_accuracy(y_hat, labels, (1, 5))
    mean_class_acc = mean_class_accuracy(y_hat, labels)
    df_cm, acc, df_scores = compute_metric_eval(y_hat, labels)
    return df_scores, df_cm, mean_class_acc, top_1_acc, top_5_acc

def find_mislabeled(pkl, mislabel_filter=None):
    labels = pkl['labels']
    y_hat = pkl['y_hat']
    preds = np.argmax(y_hat, axis=1)
    df_cm, acc, df_scores = compute_metric_eval(y_hat, labels)
    mislabeled_indices = list(np.where(preds != labels)[0])
    if mislabel_filter is not None:
        new_mislabeled_indices = []
        for i in mislabeled_indices:
            if labels[i] in mislabel_filter:
                new_mislabeled_indices.append(i)
        mislabeled_indices = new_mislabeled_indices
    else: return mislabeled_indices
    return mislabeled_indices      


# def test():
#     pkl_path = 'models/outputs/MLP.pkl'
#     df_scores = generate_model_evaluation_summary(pkl_path, 'IsoGD')

    