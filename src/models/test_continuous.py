# # -*- coding: utf-8 -*-
# Test the detector action proposal
from src.models.Detector import Detector
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import os
import pandas as pd
import numpy as np
from tqdm import tqdm
from mmcv import load, dump
from mmcv import Config, DictAction

import src.models.Detector as Detector
import src.models.Model as Model
from src.data.PoseReader import SlidingWindowFromVideo

import random 
from collections import deque

@click.command()
@click.argument('model_config_path', type=click.Path())
@click.argument('detect_model_config_path', type=click.Path())
def main(model_config_path, detect_model_config_path):
    """ 
    annotations must be a pickle file with following keys:
        detector_ts : numpy.array (labeling of action)    
    """
    # Pose estimation feed with threshold options
    DEFAULT_KPT_THRESHOLD = 0.3
    DEFAULT_BBOX_THRESHOLD = 0.3
    # Action classification threshold options
    DEFAULT_SCORE_THRESHOLD = 0.5 # Recognition model
    DEFAULT_MOVE_THRESHOLD = 10 # Recognition model
    DEFAULT_DETECT_THRESHOLD = 0.1 # Detection model
    DEFAULT_REFRACTORY_FRAME_PERIOD = 32 # After how many frames before next inf.
    SAMPLE_INTERVAL = 4

    ################################################################################
    ### Load selected model configuration ##########################################
    ### Model ######################################################################
    model_cfg = Config.fromfile(model_config_path)
    model_checkpoint = os.path.join(model_cfg.work_dir, 'latest.pth')
    ### Detector ###################################################################
    detect_model_cfg = Config.fromfile(detect_model_config_path)
    detect_model_checkpoint = os.path.join(detect_model_cfg.work_dir, 'latest.pth')
    ################################################################################

    ################################################################################
    ### Construct models ###########################################################
    detector = Detector.Detector(
        DEFAULT_DETECT_THRESHOLD, model_config_path=detect_model_config_path,
        model_checkpoint_path=detect_model_checkpoint, movement_thr=DEFAULT_MOVE_THRESHOLD
    )
    detect_sample_length = detector.model.cfg.clip_len * \
        detector.model.cfg.num_clips
    model = Model.Model(model_config_path, model_checkpoint)
    model.score_thr = DEFAULT_SCORE_THRESHOLD
    action_sample_length = model.cfg.clip_len * \
        model.cfg.num_clips

    ### Load annotation file 
    annos_path = model_cfg.ann_file_test
    annos = load(annos_path) 
    ### 
    # dict_keys(['frame_dir', 'label', 'img_shape', 'original_shape', 
    # 'total_frames', 'fps', 'keypoint', 'keypoint_score', 'bbox', 'bbox_score'])
    outputs = []
    # Run through video
    for anno in tqdm(annos, total=len(annos)):
        logging.info(anno['frame_dir'])
        detector_ts = []
        detector_output_ts = [] #  np.zeros((output['total_frames'], 2))
        detector_movement_ts = [] # np.zeros(output['total_frames'])
        model_output_ts = [] # np.zeros(output['total_frames'])
        model_output_results = []
        model_output_results_label = []
        ### Model Queues ##################################################
        detect_queue = deque(maxlen=detect_sample_length)
        model_queue = deque(maxlen=action_sample_length)
        detect_backup_pose = []
        model_backup_pose = []
        input_step = 1 # after how many frames to do inference? 
        indx = 0 # keep track of how many frames run
        frames_after_last_model_inference = 0
        ### State variables for model trigger (classifier)
        trigger_model_queue = False
        model_queue_indx = 0
        ###################################################################
        kpts = anno['keypoint']
        kpts_score = anno['keypoint_score']
        ### combine kpt and score to original output
        kpts = np.concatenate([kpts, np.expand_dims(kpts_score, axis=-1)], axis=-1)
        kpts = kpts[0]
        for frame_indx in range(0, anno['total_frames'], SAMPLE_INTERVAL):
            detector_ts.append(anno['detector_ts'][frame_indx])          
            indx += 1
            results = kpts[frame_indx:frame_indx+1]
            detect_backup_pose.append(results)
            frames_after_last_model_inference += 1 # add
            ### Detector 
            if indx == detect_sample_length:
                detect_queue.extend(detect_backup_pose)
                detect_backup_pose = []
                frames_after_last_model_inference = 0
            if (len(detect_backup_pose) == input_step) and \
                (indx > detect_sample_length):
                ### Pick a frame from the backup
                #### When the backup is full or reach the last frame
                chosen_frame = random.choice(detect_backup_pose)
                detect_queue.append(chosen_frame)
                detect_backup_pose = []
                if len(detect_queue) == detect_sample_length:
                    # Action detector to trigger and accumulate queue
                    detector_input = np.vstack(detect_queue)
                    detector_output = detector.solve_with_kpts(detector_input)
                    #detector_output = detector.solve_with_kpts(detector_input, 'motion_only')
                    detector_output_ts.append(detector.last_model_output) # 1 x 2
                    detector_movement_ts.append(detector.last_movement_value)
                    # print(anno['total_frames'], detector_input.shape, detector.last_movement_value)
                    if detector_output and \
                        (frames_after_last_model_inference > DEFAULT_REFRACTORY_FRAME_PERIOD): 
                        trigger_model_queue = True
                        frames_after_last_model_inference = 0

            ### AR Model
            if trigger_model_queue:
                ### Increase model queue until preset sample length
                model_backup_pose.append(results)

                if (len(model_backup_pose) == input_step) and \
                    (indx > action_sample_length):
                    chosen_frame = random.choice(model_backup_pose)
                    model_queue.append(chosen_frame)
                    model_queue_indx += 1
                    model_backup_pose = []

                    if model_queue_indx == action_sample_length:
                        # frames -> numpy array
                        model_input = np.vstack(model_queue)
                        # Inference
                        model_output = model.inference(
                            model_input, img_shape=(200,400),
                        )
                        model_output_results.append(model_output)
                        for i in range(action_sample_length):
                            model_output_ts.append(1)
                        label, model_output_score = model.get_action_label_wscore_from_output()
                        if model_output_score >= DEFAULT_SCORE_THRESHOLD:
                            model_output_results_label.append(label)
                        else:
                            model_output_results_label.append('None')
                           
                        # Reset triggers and indices
                        trigger_model_queue = False # reset detector
                        model_queue_indx = 0 # reset the detector index
                        frames_after_last_model_inference = 0 # reset refractory period
                        model_backup_pose = []
                        detect_backup_pose = []
            else: model_output_ts.append(0)
        ### Dictionary for output
        output = dict()
        output['frame_dir'] = anno['frame_dir']
        output['activity_labels'] = anno['activity_labels']
        output['total_frames'] = indx
        output['detector_ts'] = detector_ts
        output['detector_movement_ts'] = detector_movement_ts
        output['detector_output_ts'] = detector_output_ts
        output['model_output_results'] = model_output_results
        output['model_output_results_label'] = model_output_results_label
        output['model_output_ts'] = model_output_ts
        outputs.append(output)
        print(frame_indx, indx, len(output['activity_labels']), len(output['model_output_results']))
        dump(
            outputs,
            os.path.join('models/outputs/continuous', model.cfg.config_id+'.pkl')
        )


        # swfv = SlidingWindowFromVideo(pose_config, window_size=5, folder_path=None)
        # swfv.load_annotation(anno)

        # detector = Detector()
        # detector_ts = dict()
        # while True:
        #     _, arr = swfv.read_window()
            
        #     detect_res = detector.solve_with_kpts(arr)
        #     detector_ts[swfv.cnt_kp] = detect_res
        #     # if detector is true, then launch isolated model


        #     ### when window reaches end of video, break
        #     if swfv.cnt_kp > anno['total_frames']:
        #         break



        # print(anno['total_frames'])
        # print(detector_ts)
        # print(len(detector_ts))


    # Markup video with detector



    # # create dataloader with continuous videos
    # # The flag is used to register module's hooks
    # cfg.setdefault('module_hooks', [])
    # # build the dataloader
    # dataset = build_dataset(cfg.data.test, dict(test_mode=True))
    # dataloader_setting = dict(
    #     videos_per_gpu=cfg.data.get('videos_per_gpu', 1),
    #     workers_per_gpu=cfg.data.get('workers_per_gpu', 1),
    #     dist=distributed,
    #     shuffle=False)
    # dataloader_setting = dict(dataloader_setting,
    #                           **cfg.data.get('test_dataloader', {}))
    # data_loader = build_dataloader(dataset, **dataloader_setting)



if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]
    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    main()
