# # -*- coding: utf-8 -*-
# Train a baseline MLP model using pytorch lightning
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import numpy as np
import torch
import pytorch_lightning as pl
import torch.nn as nn
import torch.nn.functional as F

from pytorch_lightning.callbacks import LearningRateMonitor
from pytorch_lightning.loggers import TensorBoardLogger
from torchmetrics import ConfusionMatrix, F1
from torchmetrics.functional import accuracy


import os
import json
import numpy as np
from tqdm import tqdm

import mmcv

import cv2
import src.data.utils as data_utils
#import src.features.Pose as Pose


from mmaction.datasets import build_dataset, build_dataloader
from src.models.utils import mean_class_accuracy, top_k_accuracy

@click.command()
@click.argument('config', type=str)
def main(config):
    # # load config
    from mmcv import Config
    cfg = Config.fromfile(config)
    # Build a train dataloader
    from src.features.skeleton_pipelines import NanToZeros
    from src.features.skeleton_pipelines import GetMiddleFrame
    from src.features.skeleton_pipelines import SelectCondensedHandsAndArms
    from src.features.skeleton_pipelines import NormalizeSkeleton
    from src.features.skeleton_pipelines import FormatKPAndScore
    dataset = build_dataset(cfg.data.train, dict(test_mode=False))
    train_loader = build_dataloader(
            dataset,
            videos_per_gpu=cfg.data.videos_per_gpu,
            workers_per_gpu=cfg.data.workers_per_gpu,
            dist=False,
            shuffle=True)
    dataset = build_dataset(cfg.data.val, dict(test_mode=True))
    val_loader = build_dataloader(
            dataset,
            videos_per_gpu=cfg.data.videos_per_gpu,
            workers_per_gpu=cfg.data.workers_per_gpu,
            dist=False,
            shuffle=True)
    dataset = build_dataset(cfg.data.test, dict(test_mode=True))
    test_loader = build_dataloader(
            dataset,
            videos_per_gpu=cfg.data.videos_per_gpu,
            workers_per_gpu=cfg.data.workers_per_gpu,
            dist=False,
            shuffle=True)

    # ------------
    # model
    # ------------
    from src.models.mlp import MLP
    mlp = MLP(cfg.num_classes, cfg.input_dim)
    # ------------
    # training
    # ------------
    trainer = pl.Trainer(
        auto_lr_find=True, 
        auto_scale_batch_size=True, 
        gpus=1,
        deterministic=True, max_epochs=cfg.total_epochs,
        progress_bar_refresh_rate=10,
        logger=TensorBoardLogger(cfg.work_dir)
    )
    trainer.fit(mlp, train_loader, val_loader)
    # trainer.test(test_dataloaders=test_loader)
    trainer.save_checkpoint(os.path.join(cfg.work_dir, 'latest.pth'))


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    main()
