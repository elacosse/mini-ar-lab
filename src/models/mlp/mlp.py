
# import numpy as np
# from src.features.utils import normalize_skeleton_by_com
# from src.features.utils import calc_norm_keypoint_angles
# body_kpt_indices = np.arange(0, 18) # coco
# sel_body_kpt_indices = np.arange(0, 12)
# hand_kpt_indices = np.arange(91, 133)


import numpy as np
import torch
import pytorch_lightning as pl
import torch.nn as nn
import torch.nn.functional as F

from pytorch_lightning.callbacks import LearningRateMonitor
from pytorch_lightning.loggers import TensorBoardLogger
from torchmetrics import ConfusionMatrix, F1
from torchmetrics.functional import accuracy

from src.models.utils import mean_class_accuracy, top_k_accuracy

class MLP(pl.LightningModule):

    def __init__(self, n_classes, input_dim):
        super().__init__()
        self.n_classes = n_classes
        self.input_dim = input_dim
        self.y = {'val' : None, 'test': None}
        self.y_hat = {'val' : None, 'test': None}

        # self._confmat = ConfusionMatrix(n_classes)
        # self.last_val_confusion_matrix = None
        # self._f1 = F1(num_classes=self.n_classes)
        # self.last_val_f1 = None

        self.layers = nn.Sequential(
            nn.Linear(input_dim, 64), 
            nn.ReLU(),
            # nn.Linear(128, 64),
            # nn.ReLU(),
            nn.Linear(64, 32),
            nn.ReLU(),
            nn.Linear(32, n_classes)
        )

    def forward(self, x):
        x = x.view(x.size(0), -1)
        return self.layers(x)

    def cross_entropy_loss(self, logits, labels):
        # if self.n_classes == 2:
        #     return F.binary_cross_entropy(logits, labels)
        # else:
        return F.cross_entropy(logits, labels)
        # return F.nll_loss(logits, labels)

    def training_step(self, batch, batch_idx):
        x = batch['pose']
        y = batch['label']
        x = x.view(x.size(0), -1)
        x = torch.nan_to_num(x)
        y_hat = self.layers(x)
        # loss - cross entropy
        loss = self.cross_entropy_loss(y_hat, y)
        self.log('train_loss', loss)
        return loss
    
    def evaluate(self, batch, stage=None):
        x = batch['pose']
        # convert tensor (128, 1, 28, 28) --> (128, 1*28*28)
        x = x.view(x.size(0), -1)
        x = torch.nan_to_num(x)
        y = batch['label']      
        y_hat = self.layers(x)
        loss = self.cross_entropy_loss(y_hat, y)
        # preds = torch.argmax(y_hat, dim=1)
        # acc = accuracy(preds, y)
        if stage:
            labels = y.detach().cpu().numpy()
            yhat = y_hat.detach().cpu().numpy()
            top1_acc, top5_acc = top_k_accuracy(yhat,labels, (1, 5))
            m_acc = mean_class_accuracy(yhat, labels)
            self.log(f'{stage}_loss', loss, prog_bar=True)
            self.log(f'{stage}_mean_class_acc', m_acc, prog_bar=True)
            self.log(f'{stage}_top_1_acc', top1_acc, prog_bar=True)
            self.log(f'{stage}_top_5_acc', top5_acc, prog_bar=True)
        return y_hat, y

    def validation_step(self, batch, batch_idx):
        y_hat, y = self.evaluate(batch, 'val')
        return {'yhat': y_hat, 'target': y}

    def validation_epoch_end(self, outputs):
        #preds = torch.cat([tmp['preds'] for tmp in outputs])
        y_hat = torch.cat([tmp['yhat'] for tmp in outputs])
        targets = torch.cat([tmp['target'] for tmp in outputs])
        #confusion_matrix = self._confmat(preds, targets)
        #self.last_val_confusion_matrix = np.nan_to_num(confusion_matrix.cpu())
        #self.last_val_f1 = self._f1(preds, targets)
        # store outputs
        self.y_hat['val'] = y_hat
        self.y['val'] = targets
        y_hat = self.y_hat['val'].detach().cpu().numpy()
        labels = self.y['val'].detach().cpu().numpy()
        top1_acc, top5_acc = top_k_accuracy(y_hat, labels, (1, 5))
        m_acc = mean_class_accuracy(y_hat, labels)
        
    # def test_step(self, batch, batch_idx):
    #     y_hat, y = self.evaluate(batch, 'test')
    #     return {'yhat': y_hat, 'target': y}
  
    # def test_epoch_end(self, outputs):
    #     #preds = torch.cat([tmp['preds'] for tmp in outputs])
    #     y_hat = torch.cat([tmp['yhat'] for tmp in outputs])
    #     targets = torch.cat([tmp['target'] for tmp in outputs])
    #     #confusion_matrix = self._confmat(preds, targets)
    #     #self.last_val_confusion_matrix = np.nan_to_num(confusion_matrix.cpu())
    #     #self.last_val_f1 = self._f1(preds, targets)
    #     # store outputs
    #     self.y_hat['val'] = y_hat
    #     self.y['val'] = targets
    #     y_hat = self.y_hat['val'].detach().cpu().numpy()
    #     labels = self.y['val'].detach().cpu().numpy()
    #     top1_acc, top5_acc = top_k_accuracy(y_hat, labels, (1, 5))
    #     m_acc = mean_class_accuracy(y_hat, labels)
    #     self.log('test_top_1_acc', top1_acc)
    #     self.log('test_top_5_acc', top5_acc)
    #     self.log('test_mean_acc', m_acc)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-4)
        return optimizer

# def process_kpt(kpts):
#     # # check if body keypoints are intact via score
#     # sel_body_kpt_indices = \
#     #     body_kpt_indices[kpts_score[body_kpt_indices] > score_threshold]
#     norm_kpts = normalize_skeleton_by_com(kpts, body_indices=sel_body_kpt_indices)
#     # kpt_angles = calc_norm_keypoint_angles(norm_kpts)
#     # kpt_angles = np.nan_to_num(kpt_angles)

#     # select body and hands and offset indices if thresholded
#     adj_hands_kpt_indices = np.arange(91, 134) - \
#         (len(body_kpt_indices)-len(sel_body_kpt_indices))
#     sel_kpts_indices = np.concatenate([sel_body_kpt_indices, adj_hands_kpt_indices])
#     #x = kpt_angles[sel_kpts_indices]
#     x = norm_kpts[sel_kpts_indices]
#     return np.nan_to_num(x.flatten())