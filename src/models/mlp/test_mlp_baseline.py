# # -*- coding: utf-8 -*-
# Train a baseline MLP model using pytorch lightning
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv

import torch
from pytorch_lightning.callbacks import ModelCheckpoint
import pytorch_lightning as pl
from src.models.utils import mean_class_accuracy, top_k_accuracy

# from torch.utils.data import Dataset, DataLoader
# import matplotlib.pyplot as plt
# import seaborn as sns
# import pandas as pd


import os
import json
import numpy as np
from tqdm import tqdm

import mmcv

import cv2
import src.data.utils as data_utils
#import src.features.Pose as Pose
import pickle 

from mmaction.datasets import build_dataset, build_dataloader

@click.command()
@click.argument('config', type=str)
def main(config):
    # # load config
    from mmcv import Config
    cfg = Config.fromfile(config)
    config_id = cfg.config_id

    # Build a train dataloader
    from src.features.skeleton_pipelines import NanToZeros
    from src.features.skeleton_pipelines import GetMiddleFrame
    from src.features.skeleton_pipelines import SelectCondensedHandsAndArms
    from src.features.skeleton_pipelines import NormalizeSkeleton
    from src.features.skeleton_pipelines import FormatKPAndScore
    # dataset = build_dataset(cfg.data.val, dict(test_mode=True))
    # val_loader = build_dataloader(
    #     dataset,
    #     videos_per_gpu=cfg.data.videos_per_gpu,
    #     workers_per_gpu=cfg.data.workers_per_gpu,
    #     dist=False,
    #     shuffle=False
    # )
    dataset = build_dataset(cfg.data.test, dict(test_mode=True))
    test_loader = build_dataloader(
        dataset,
        videos_per_gpu=cfg.data.videos_per_gpu,
        workers_per_gpu=cfg.data.workers_per_gpu,
        dist=False,
        shuffle=False
    )
    # loaders = {'valid': val_loader, 'test': test_loader}
    loaders = {'test': test_loader}
    # ------------
    # model
    # ------------
    from src.models.mlp import MLP
    mlp = MLP(cfg.num_classes, cfg.input_dim)
    model = mlp.load_from_checkpoint(n_classes=cfg.num_classes, 
        input_dim=cfg.input_dim,
        checkpoint_path=os.path.join(cfg.work_dir, 'latest.pth'))
    model.eval()

    for cv_id in loaders.keys():
        loader = loaders[cv_id]
        # Save model outputs
        y_hat_outputs = []
        y_labels = []
        with torch.no_grad():
            # # Iterate over the test data and generate predictions
            for i_batch, sample_batched in enumerate(loader):
                # inputs = sample_batched['pose']
                # labels = sample_batched['label']
                # Generate outputs
                y_hat, y = model.evaluate(sample_batched)
                y_hat_outputs.append(y_hat)
                y_labels.append(y)

        # Send through evaluation summary
        y_hat = torch.vstack(y_hat_outputs)
        y = torch.cat(y_labels)
        labels = y.detach().cpu().numpy()
        yhat = y_hat.detach().cpu().numpy()
        # save outputs to output directory 
        output = {
            'model_id': config_id,
            'cv_id' : cv_id,
            'labels' : labels,
            'y_hat' : yhat,
            }
        pkl_output_path_temp = os.path.join(project_dir,
             'models/outputs', config_id+'_cv-{cv_id}.pkl'
        )
        with open(pkl_output_path_temp.format(cv_id=cv_id), 'wb') as handle:
            pickle.dump(output, handle, protocol=pickle.HIGHEST_PROTOCOL)
        
        top_1_acc, top_5_acc = top_k_accuracy(yhat,labels, (1, 5))
        mean_class_acc = mean_class_accuracy(yhat, labels)
        print(f'Mean Class Accuracy: {mean_class_acc:.04f}')
        print(f'Top 1 Accuracy: {top_1_acc:.04f}')
        print(f'Top 5 Accuracy: {top_5_acc:.04f}')

        
if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    main()

