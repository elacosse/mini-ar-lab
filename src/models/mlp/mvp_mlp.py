
import numpy as np

def calc_norm_keypoint_angles(keypoints):
    angles = [np.arctan2(keypoint[1], keypoint[0]) % (2 * np.pi) 
        for keypoint in keypoints]
    return np.array(angles)


def normalize_skeleton_by_com(keypoint, body_indices=list(range(0, 18)),
        scaling_bodypart='shoulder'):
    """Normalizes skeleton making it translational and scale invariant.
    Translation: center of mass
    Scale: shoulder width
    """
    assert(keypoint[0,0] != np.nan)

    if scaling_bodypart == 'shoulder':
        scaling_kp_indx_1 = 5 # right shoulder
        scaling_kp_indx_2 = 6 # left shoulder
    scaling_kp_1 = keypoint[scaling_kp_indx_1]
    scaling_kp_2 = keypoint[scaling_kp_indx_2]
    dist = np.abs(scaling_kp_1[0:2]-scaling_kp_2[0:2]) # maybe check body orientation via without abs?
    norm_dist = np.linalg.norm(dist) 

    # select 17 body parts for COM calculation
    keypoint_body = keypoint[body_indices, 0:2] # coco 17 body
    xy_bar = np.array([np.mean(keypoint_body[:,0]), 
        np.mean(keypoint_body[:,1])])
    trans_kp = xy_bar
    norm_keypoint = np.copy(keypoint)
    for kp_indx in range(len(keypoint)):
        norm_keypoint[kp_indx,0:2] = (keypoint[kp_indx,0:2]-trans_kp)/norm_dist
    return norm_keypoint





body_kpt_indices = np.arange(0, 18) # coco
sel_body_kpt_indices = np.arange(0, 12)
hand_kpt_indices = np.arange(91, 133)


from torch import nn
import pytorch_lightning as pl
from torch.nn import functional as F
class MLP(pl.LightningModule):
  
    def __init__(self, n_classes):
        super().__init__()
        self.layers = nn.Sequential(
            nn.Linear(8 * 165, 64),
            nn.ReLU(),
            nn.Linear(64, 32),
            nn.ReLU(),
            nn.Linear(32, n_classes)
        )

    def forward(self, x):
        return self.layers(x)

    def cross_entropy_loss(self, logits, labels):
        return F.cross_entropy(logits, labels)
        #return F.nll_loss(logits, labels)
    
    def training_step(self, batch, batch_idx):
        x, y = batch
        x = x.view(x.size(0), -1)
        y_hat = self.layers(x)
        loss = self.cross_entropy_loss(y_hat, y)
        self.log('train_loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        x = x.view(x.size(0), -1)
        y_hat = self.layers(x)
        loss = self.cross_entropy_loss(y_hat, y)
        self.log('val_loss', loss)
        return loss
  
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-4)
        return optimizer


def process_kpt(kpts):
    # # check if body keypoints are intact via score
    # sel_body_kpt_indices = \
    #     body_kpt_indices[kpts_score[body_kpt_indices] > score_threshold]
    norm_kpts = normalize_skeleton_by_com(kpts, body_indices=sel_body_kpt_indices)
    # kpt_angles = calc_norm_keypoint_angles(norm_kpts)
    # kpt_angles = np.nan_to_num(kpt_angles)

    # select body and hands and offset indices if thresholded
    adj_hands_kpt_indices = np.arange(91, 134) - \
        (len(body_kpt_indices)-len(sel_body_kpt_indices))
    sel_kpts_indices = np.concatenate([sel_body_kpt_indices, adj_hands_kpt_indices])
    #x = kpt_angles[sel_kpts_indices]
    x = norm_kpts[sel_kpts_indices]
    return np.nan_to_num(x.flatten())


