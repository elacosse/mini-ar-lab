# # -*- coding: utf-8 -*-
# Model class for performing action recognition
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import json

import torch
import numpy as np
from mmcv import Config, DictAction
from mmcv.parallel import collate, scatter

from mmaction.datasets.pipelines import Compose
EXCLUDE_STEPS = [
    'OpenCVInit', 'OpenCVDecode', 'DecordInit', 'DecordDecode', 'PyAVInit',
    'PyAVDecode', 'RawFrameDecode'
]
############################################################################
try: 
    from src.features.skeleton_pipelines import NanToZeros
    from src.features.skeleton_pipelines import SelectCondensedHandsAndArms
    from src.features.skeleton_pipelines import NormalizeSkeleton
except Exception as e:
    print(e)
############################################################################
import mmcv
from mmaction.models import build_recognizer
from mmcv.runner import load_checkpoint
# from mmaction.apis import init_recognizer
def init_recognizer(config,
                    checkpoint=None,
                    device='cuda:0',
                    use_frames=False):
    """Initialize a recognizer from config file.

    Args:
        config (str | :obj:`mmcv.Config`): Config file path or the config
            object.
        checkpoint (str | None, optional): Checkpoint path/url. If set to None,
            the model will not load any weights. Default: None.
        device (str | :obj:`torch.device`): The desired device of returned
            tensor. Default: 'cuda:0'.
        use_frames (bool): Whether to use rawframes as input. Default:False.

    Returns:
        nn.Module: The constructed recognizer.
    """
    if isinstance(config, str):
        config = mmcv.Config.fromfile(config)
    elif not isinstance(config, mmcv.Config):
        raise TypeError('config must be a filename or Config object, '
                        f'but got {type(config)}')
    if ((use_frames and config.dataset_type != 'RawframeDataset')):
            # or (not use_frames and config.dataset_type != 'VideoDataset')):
        input_type = 'rawframes' if use_frames else 'video'
        raise RuntimeError('input data type should be consist with the '
                           f'dataset type in config, but got input type '
                           f"'{input_type}' and dataset type "
                           f"'{config.dataset_type}'")

    # pretrained model is unnecessary since we directly load checkpoint later
    config.model.backbone.pretrained = None
    model = build_recognizer(config.model, test_cfg=config.get('test_cfg'))

    if checkpoint is not None:
        load_checkpoint(model, checkpoint, map_location=device)
    model.cfg = config
    model.to(device)
    model.eval()
    return model

###################################################################
from pytorch_lightning.callbacks import ModelCheckpoint
import pytorch_lightning as pl
from src.models.utils import mean_class_accuracy, top_k_accuracy
###################################################################
class Model(object):
    def __init__(self, config_path, checkpoint_path, device='cuda:0'):
        """Model object class for performing action recognition

        Args:
            config_path (str): Path to config file
            checkpoint_path (str): Path to checkpoint file
            device (str): Device to use. Default: 'cuda:0'

        """
        self.cfg = Config.fromfile(config_path) # model config
        self.device = device # device id
        self.checkpoint_path = checkpoint_path # .pth file path
        self.model = None 
        self.test_pipeline = None 
        self.data = None 
        self.score_thr = 0
        self.last_output = None 
        # Load labels if in configuration file
        # print(self.cfg.keys())
        if 'labels_from' in list(self.cfg.keys()):
            with open(self.cfg.labels_from) as f:
                self.labels = list(json.load(f).values()) # list of strings from dict
        else: self.labels = []
        # Construct data pipeline
        self.sample_length = 0
        self.num_clips = 0
        self.clip_len = 0
        pipeline = self.cfg.data.test.pipeline
        pipeline_ = pipeline.copy()

        ### remove unnecessary dataloader pipeline steps
        for step in pipeline:
            if 'SampleFrames' in step['type']:
                self.clip_len = step['clip_len']
                self.num_clips = step['num_clips']
                self.sample_length = step['clip_len'] * step['num_clips']
                pipeline_.remove(step)
            if step['type'] in EXCLUDE_STEPS:
                ### remove step to decode frames
                pipeline_.remove(step)
        self.test_pipeline = Compose(pipeline_)   

        if self.cfg.model_type == 'pose':
            self.data = dict(img_shape=None, modality='PoseDataset', label=-1)
            self.data['num_clips'] = self.num_clips
            self.data['clip_len'] = self.clip_len

            if self.cfg.model_name == 'Pose-MLP':
                from src.models.mlp.mlp import MLP
                mlp = MLP(self.cfg.num_classes, self.cfg.input_dim)
                self.model = mlp.load_from_checkpoint(
                    n_classes=self.cfg.num_classes, 
                    input_dim=self.cfg.input_dim,
                    checkpoint_path=self.checkpoint_path
                )
                self.model.eval()

            elif self.cfg.model_name == 'Posec3D':
                self.model = init_recognizer(
                    self.cfg, self.checkpoint_path, device=self.device,
                    use_frames=False,
                )


        elif self.cfg.model_type == 'rgb': # feed in raw rgb to do classification
            # TODO ...
            self.model = init_recognizer(
                self.cfg, self.checkpoint_path, device=self.device
            )

    def get_action_label_wscore_from_output(self):
        """From model output, returns best score and label"""
        argmax_indx = np.argmax(self.last_output)
        label = self.labels[argmax_indx]
        score = self.last_output[argmax_indx]
        return label, score

    def inference(self, input, img_shape=None):
        """Performs forward inference with selected model and returns raw output"""
        ### MLP lightning model
        if self.cfg.model_name == 'Pose-MLP':
            if self.cfg.clip_len == 1:
                # Take middle frame
                middle_frame_indx = int(input.shape[0]/2)
                # 1x133x2
                kpt = input[middle_frame_indx:middle_frame_indx+1][:,:,0:2]
                # 1x133
                kpt_score = input[middle_frame_indx:middle_frame_indx+1][:,:,2]
            else:
                kpt = input[:,:,0:2]
                kpt_score = input[:,:,2]

            cur_data = self.data.copy()
            cur_data['total_frames'] = self.cfg.clip_len
            cur_data['keypoint'] = np.expand_dims(kpt, axis=0)
            cur_data['keypoint_score'] = np.expand_dims(kpt_score, axis=0)
            x = self.test_pipeline(cur_data) # preproc
            with torch.no_grad():
                output = self.model.forward(x['pose'])
                output = output.numpy()
                if len(output) == 1:
                    output = output[0]
                self.last_output = output
                return output

        elif self.cfg.model_name == 'Posec3D':
            assert img_shape != None
            kpt = input[:][:,:,0:2]
            kpt_score = input[:][:,:,2]
            cur_data = self.data.copy()
            cur_data['img_shape'] = img_shape
            cur_data['total_frames'] = len(input)
            cur_data['keypoint'] = np.expand_dims(kpt, axis=0)
            cur_data['keypoint_score'] = np.expand_dims(kpt_score, axis=0)
            x = self.test_pipeline(cur_data)
            x = collate([x], samples_per_gpu=1)
            x = scatter(x, [self.device])[0]
            with torch.no_grad():
                output = self.model(return_loss=False, **x)[0]
                self.last_output = output
                return output
                
        # mmaction rgb based model
        elif self.cfg.model_type == 'rgb': 
            # TODO
            pass

        return output
