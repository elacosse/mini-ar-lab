# _base_ = ['/workspace/git/mmaction2/configs/recognition/i3d/i3d_r50_32x2x1_100e_kinetics400_rgb.py']
#_base_ = ['/home/lugo/eric/ml-workspace/git/mmaction2/configs/recognition/i3d/i3d_r50_32x2x1_100e_kinetics400_rgb.py']
_base_ = [
    '/workspace/git/mmaction2/configs/_base_/models/i3d_r50.py', '/workspace/git/mmaction2/configs/_base_/schedules/sgd_100e.py',
    '/workspace/git/mmaction2/configs/_base_/default_runtime.py'
]

# config
config_id = 'i3d_r50_video_32x2x1_100e_kinetics400_rgb_data-SLR'
model_name = 'i3d'
model_type = 'rgb'
data_id = 'SLR'
clip_len = 32
frame_interval = 2
num_clips = 1
total_epochs = 100
videos_per_gpu = 32
workers_per_gpu = 8
num_classes = 226

# model settings
model = dict(
    cls_head=dict(num_classes=num_classes)
)



# dataset settings
dataset_type = 'VideoDataset'
data_root = 'data/raw/SLR'
data_root_val = 'data/raw/SLR'

ann_file_train = 'data/processed/SLR/annotations_226/train_rgb.txt'
ann_file_val = 'data/processed/SLR/annotations_226/valid_rgb.txt'
ann_file_test = 'data/processed/SLR/annotations_226/test_rgb.txt'

# Config of image normalization used in data pipeline
img_norm_cfg = dict(
    mean=[123.675, 116.28, 103.53], 
    std=[58.395, 57.12, 57.375], 
    to_bgr=False)

train_pipeline = [
    dict(type='DecordInit'),
    dict(
        type='SampleFrames', 
        clip_len=clip_len, 
        frame_interval=frame_interval, 
        num_clips=num_clips
        ),
    dict(type='DecordDecode'),
    dict(type='Resize', scale=(-1, 256)),
    dict(
        type='MultiScaleCrop',
        input_size=224,
        scales=(1, 0.8),
        random_crop=False,
        max_wh_scale_gap=0),
    dict(type='Resize', scale=(224, 224), keep_ratio=False),
    dict(type='Flip', flip_ratio=0.5),
    dict(type='Normalize', **img_norm_cfg),
    dict(type='FormatShape', input_format='NCTHW'),
    dict(type='Collect', keys=['imgs', 'label'], meta_keys=[]),
    dict(type='ToTensor', keys=['imgs', 'label'])
]
val_pipeline = [
    dict(type='DecordInit'),
    dict(
        type='SampleFrames', 
        clip_len=clip_len, 
        frame_interval=frame_interval, 
        num_clips=num_clips,
        test_mode=True
        ),
    dict(type='DecordDecode'),
    dict(type='Resize', scale=(-1, 256)),
    dict(type='CenterCrop', crop_size=224),
    dict(type='Normalize', **img_norm_cfg),
    dict(type='FormatShape', input_format='NCTHW'),
    dict(type='Collect', keys=['imgs', 'label'], meta_keys=[]),
    dict(type='ToTensor', keys=['imgs'])
]
test_pipeline = [
    dict(type='DecordInit'),
    dict(
        type='SampleFrames', 
        clip_len=clip_len, 
        frame_interval=frame_interval, 
        num_clips=num_clips,
        test_mode=True
        ),
    dict(type='DecordDecode'),
    dict(type='Resize', scale=(-1, 256)),
    dict(type='ThreeCrop', crop_size=256),
    dict(type='Normalize', **img_norm_cfg),
    dict(type='FormatShape', input_format='NCTHW'),
    dict(type='Collect', keys=['imgs', 'label'], meta_keys=[]),
    dict(type='ToTensor', keys=['imgs'])
]
data = dict(
    videos_per_gpu=videos_per_gpu,
    workers_per_gpu=workers_per_gpu,
    train=dict(
        type=dataset_type,
        ann_file=ann_file_train,
        data_prefix=data_root,
        pipeline=train_pipeline),
    val=dict(
        type=dataset_type,
        ann_file=ann_file_val,
        data_prefix=data_root,
        pipeline=val_pipeline),
    test=dict(
        type=dataset_type,
        ann_file=ann_file_val,
        data_prefix=data_root,
        pipeline=test_pipeline))

# runtime settings
work_dir = './work_dirs/' + config_id

evaluation = dict(  # Config of evaluation during training
    interval=1,  # Interval to perform evaluation
    metrics=['top_k_accuracy', 'mean_class_accuracy'],  # Metrics to be performed
    metric_options=dict(top_k_accuracy=dict(topk=(1, 5))), # Set top-k accuracy to 1 and 5 during validation
    save_best='auto', # set `top_k_accuracy` as key indicator to save best checkpoint 
)

checkpoint_config = dict(  # Config to set the checkpoint hook, Refer to https://github.com/open-mmlab/mmcv/blob/master/mmcv/runner/hooks/checkpoint.py for implementation
    interval=4, # Interval to save checkpoint
    save_last=True,
)

log_config = dict(  # Config to register logger hook
    interval=20,  # Interval to print the log on batch
    hooks=[  # Hooks to be implemented during training
        dict(type='TextLoggerHook'),  # The logger used to record the training process
        dict(type='TensorboardLoggerHook'),  # The Tensorboard logger is also supported
    ])

dist_params = dict(backend='nccl')

log_level = 'INFO'
use_pretrained = True
load_from = None
resume_from = work_dir + '/latest.pth'
#resume_from = None
workflow = [('train', 1), ('val', 1)]

# runtime settings
seed = 0
#gpu_ids = None
omnisource = False
find_unused_parameters = False



