config_id = 'posec_32x2x1_IsoGD249_data-IsoGD_11'
data_id = 'IsoGD_11'
labels_from = 'data/raw/mini-ar-lab/selected_labels.json'
model_name = 'Posec3D'
model_type = 'pose'
clip_len = 32
frame_interval = 2
num_clips = 1
total_epochs = 100
videos_per_gpu = 16
workers_per_gpu = 8

in_channels = 26 # 27 # 53
num_classes = 11

model = dict(
    type='Recognizer3D',
    backbone=dict(
        type='ResNet3dSlowOnly',
        depth=50,
        pretrained=None,
        in_channels=in_channels,
        base_channels=32,
        num_stages=3,
        out_indices=(2, ),
        stage_blocks=(4, 6, 3),
        conv1_stride_s=1,
        pool1_stride_s=1,
        inflate=(0, 1, 1),
        spatial_strides=(2, 2, 2),
        temporal_strides=(1, 1, 2),
        dilations=(1, 1, 1)),
    cls_head=dict(
        type='I3DHead',
        in_channels=512,
        num_classes=num_classes,
        spatial_type='avg',
        dropout_ratio=0.5),
    train_cfg=dict(),
    test_cfg=dict(average_clips='prob'))

dataset_type = 'PoseDataset'
ann_file_train = './data/processed/IsoGD/annotations_' + str(num_classes) + '/train_skeleton.pkl'
ann_file_val = './data/processed/IsoGD/annotations_' + str(num_classes) + '/valid_skeleton.pkl'
ann_file_test = './data/processed/IsoGD/annotations_' + str(num_classes) + '/test_skeleton.pkl'

# Bones and selected keypoints (hands and arms condensed from wbcoco)
sel_bones = (
            (5, 6), (5, 7),
            (6, 8), (8, 10), (7, 9), (9, 11), 
            (12,13),(12,14),(12,16),(12,18),(12,20),
            (14,15),(16,17),(18,19),(20,21),
            (22,23),(22,24),(22,26),(22,28),(22,30),
            (24,25),(26,27),(28,29),(30,31),
            (10,12),(11,22)
        )
left_kp = [1,3,5] + [7,8,9,10,11,12,13,14,15,16]
right_kp = [2,4,5] + [17,18,19,20,21,22,23,24,25,26]
skeletons = [(bone[0]-5, bone[1]-5) for bone in sel_bones]

# #########################################################################

reshape_img_size = 128
train_pipeline = [
    dict(type='NanToZeros'),
    dict(type='SelectCondensedHandsAndArms'), # selects only 27 keypoints
    dict(
        type='SampleFrames', 
        clip_len=clip_len, 
        frame_interval=frame_interval, 
        num_clips=num_clips
        ),
    # dict(type='UniformSampleFrames', 
    #     clip_len=clip_len, 
    #     num_clips=num_clips),
    dict(type='PoseDecode'),
    dict(type='PoseCompact', hw_ratio=1., allow_imgpad=True),
    dict(type='Resize', scale=(-1, reshape_img_size)),
    dict(type='CenterCrop', crop_size=reshape_img_size),
    dict(
        type='GeneratePoseTarget',
        sigma=0.6,
        use_score=True,
        with_kp=False,
        with_limb=True,
        skeletons=skeletons,
        left_kp=left_kp,
        right_kp=right_kp,
        ),
    dict(type='FormatShape', input_format='NCTHW'),
    dict(type='Collect', keys=['imgs', 'label'], meta_keys=[]),
    dict(type='ToTensor', keys=['imgs'])
]
val_pipeline = [
    dict(type='NanToZeros'),
    dict(type='SelectCondensedHandsAndArms'), # selects only 27 keypoints
    dict(
        type='SampleFrames', 
        clip_len=clip_len, 
        frame_interval=frame_interval, 
        num_clips=num_clips
        ),
    dict(type='PoseDecode'),
    dict(type='PoseCompact', hw_ratio=1., allow_imgpad=True),
    dict(type='Resize', scale=(-1, reshape_img_size)),
    dict(type='CenterCrop', crop_size=reshape_img_size),
    dict(
        type='GeneratePoseTarget',
        sigma=0.6,
        use_score=True,
        with_kp=False,
        with_limb=True,
        skeletons=skeletons,
        left_kp=left_kp,
        right_kp=right_kp,
        ),
    dict(type='FormatShape', input_format='NCTHW'),
    dict(type='Collect', keys=['imgs', 'label'], meta_keys=[]),
    dict(type='ToTensor', keys=['imgs'])
]
test_pipeline = [
    dict(type='NanToZeros'),
    dict(type='SelectCondensedHandsAndArms'), # selects only 27 keypoints
    dict(
        type='SampleFrames', 
        clip_len=clip_len, 
        frame_interval=frame_interval, 
        num_clips=num_clips
        ),
    dict(type='PoseDecode'),
    dict(type='PoseCompact', hw_ratio=1., allow_imgpad=True),
    dict(type='Resize', scale=(-1, reshape_img_size)),
    dict(type='CenterCrop', crop_size=reshape_img_size),
    dict(
        type='GeneratePoseTarget',
        sigma=0.6,
        use_score=True,
        with_kp=False,
        with_limb=True,
        skeletons=skeletons,
        left_kp=left_kp,
        right_kp=right_kp,
        ),
    dict(type='FormatShape', input_format='NCTHW'),
    dict(type='Collect', keys=['imgs', 'label'], meta_keys=[]),
    dict(type='ToTensor', keys=['imgs'])
]
data = dict(
    videos_per_gpu=videos_per_gpu,
    workers_per_gpu=workers_per_gpu,
    test_dataloader=dict(videos_per_gpu=1),
    train=dict(
        type=dataset_type,
        ann_file=ann_file_train,
        data_prefix='',
        pipeline=train_pipeline),
    val=dict(
        type=dataset_type,
        ann_file=ann_file_val,
        data_prefix='',
        pipeline=val_pipeline),
    test=dict(
        type=dataset_type,
        ann_file=ann_file_test,
        data_prefix='',
        pipeline=test_pipeline))
# optimizer
optimizer = dict(
    type='SGD', lr=0.2/8, momentum=0.9,
    weight_decay=0.0001)  # this lr is used for 8 gpus
optimizer_config = dict(grad_clip=dict(max_norm=40, norm_type=2))
# learning policy
lr_config = dict(policy='CosineAnnealing', by_epoch=False, min_lr=0)


# runtime settings
work_dir = './work_dirs/' + config_id

checkpoint_config = dict(  # Config to set the checkpoint hook, Refer to https://github.com/open-mmlab/mmcv/blob/master/mmcv/runner/hooks/checkpoint.py for implementation
    interval=2)  # Interval to save checkpoint

evaluation = dict(  # Config of evaluation during training
    interval=1,  # Interval to perform evaluation
    metrics=['top_k_accuracy', 'mean_class_accuracy'],  # Metrics to be performed
    metric_options=dict(top_k_accuracy=dict(topk=(1, 5))), # Set top-k accuracy to 1 and 5 during validation
    save_best='auto'
)

log_config = dict(  # Config to register logger hook
    interval=20,  # Interval to print the log on batch
    hooks=[  # Hooks to be implemented during training
        dict(type='TextLoggerHook'),  # The logger used to record the training process
        dict(type='TensorboardLoggerHook'),  # The Tensorboard logger is also supported
    ])

dist_params = dict(backend='nccl')

log_level = 'INFO'
use_pretrained = True
# load_from = None
load_from = '/workspace/git/mini-ar-lab/work_dirs/posec_32x2x1_SLR_data-IsoGD_249/latest.pth'
# resume_from = None
# resume_from = work_dir + '/latest.pth'

workflow = [('train', 1), ('val', 1)]

# runtime settings
seed = 0
gpu_ids = range(4)
omnisource = False
find_unused_parameters = False
