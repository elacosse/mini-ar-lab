config_id = 'mlp_pose_1x1x1_150e_data-SLR'
model_name = 'Pose-MLP'
model_type = 'pose'
pose_config_file = 'wbcoco_mmpose_config.yml'
data_id = 'SLR'
num_classes = 226
input_dim = 27*3 # 1296 #2592
total_epochs = 150
dataset_type = 'PoseDataset'
data_root  = 'data/processed/SLR'
ann_file_train = 'annotations_226/train_skeleton_static.pkl'
ann_file_val = 'annotations_226/valid_skeleton_static.pkl'
ann_file_test = 'annotations_226/test_skeleton_static.pkl'

import os
onk = '/workspace/git/mini-ar-lab'
ann_file_train = os.path.join(onk, data_root, ann_file_train)
ann_file_val = os.path.join(onk, data_root, ann_file_val)
ann_file_test = os.path.join(onk, data_root, ann_file_test)

# Bones and selected keypoints (hands and arms condensed from wbcoco)
sel_bones = (
            (5, 6), (5, 7),
            (6, 8), (8, 10), (7, 9), (9, 11), 
            (12,13),(12,14),(12,16),(12,18),(12,20),
            (14,15),(16,17),(18,19),(20,21),
            (22,23),(22,24),(22,26),(22,28),(22,30),
            (24,25),(26,27),(28,29),(30,31),
            (10,12),(11,22)
        )
left_kp = [1,3,5] + [7,8,9,10,11,12,13,14,15,16]
right_kp = [2,4,5] + [17,18,19,20,21,22,23,24,25,26]
skeletons = [(bone[0]-5, bone[1]-5) for bone in sel_bones]

##########################################################################
clip_len = 1
frame_interval = 1
num_clips = 1
reshape_img_size = 128
train_pipeline = [
    dict(type='NormalizeSkeleton'),
    dict(type='SelectCondensedHandsAndArms'), # selects only 27 keypoints
    #dict(type='Flip', flip_ratio=0.5, left_kp=left_kp, right_kp=right_kp),
    dict(
        type='SampleFrames', 
        clip_len=clip_len, 
        frame_interval=frame_interval, 
        num_clips=num_clips
        ),
    dict(type='PoseDecode'),
    #dict(type='PoseCompact', hw_ratio=1., allow_imgpad=True),
    #dict(type='FormatShape', input_format='NCTHW'),
    dict(type='FormatKPAndScore'),
    dict(type='Collect', keys=['pose', 'label'], meta_keys=['frame_dir']),
    dict(type='ToTensor', keys=['pose'])
]
val_pipeline = [
    dict(type='NormalizeSkeleton'),
    dict(type='SelectCondensedHandsAndArms'), # selects only 27 keypoints
    dict(
        type='SampleFrames', 
        clip_len=clip_len, 
        frame_interval=frame_interval, 
        num_clips=num_clips
        ),
    dict(type='PoseDecode'),
    #dict(type='PoseCompact', hw_ratio=1., allow_imgpad=True),
    #dict(type='FormatShape', input_format='NCTHW'),
    dict(type='FormatKPAndScore'),
    dict(type='Collect', keys=['pose', 'label'], meta_keys=['frame_dir']),
    dict(type='ToTensor', keys=['pose'])
]
test_pipeline = [
    dict(type='NormalizeSkeleton'),
    dict(type='SelectCondensedHandsAndArms'), # selects only 27 keypoints
    dict(
        type='SampleFrames', 
        clip_len=clip_len, 
        frame_interval=frame_interval, 
        num_clips=num_clips
        ),
    dict(type='PoseDecode'),
    dict(type='FormatKPAndScore'),
    dict(type='Collect', keys=['pose', 'label'], meta_keys=[]),
    dict(type='ToTensor', keys=['pose'])
]

data = dict(
    videos_per_gpu=256,
    workers_per_gpu=8,
    test_dataloader=dict(videos_per_gpu=10),
    train=dict(
        type=dataset_type,
        ann_file=ann_file_train,
        data_prefix=data_root,
        pipeline=train_pipeline),
    val=dict(
        type=dataset_type,
        ann_file=ann_file_val,
        data_prefix=data_root,
        pipeline=val_pipeline),
    test=dict(
        type=dataset_type,
        ann_file=ann_file_test,
        data_prefix=data_root,
        pipeline=test_pipeline))
# # optimizer
# optimizer = dict(
#     type='SGD', lr=0.2, momentum=0.9,
#     weight_decay=0.0001)  # this lr is used for 8 gpus
# optimizer_config = dict(grad_clip=dict(max_norm=40, norm_type=2))
# # learning policy
# lr_config = dict(policy='CosineAnnealing', by_epoch=False, min_lr=0)
checkpoint_config = dict(interval=2)
workflow = [('train', 1), ('val', 1)]
evaluation = dict(
    interval=10,
    metrics=['top_k_accuracy', 'mean_class_accuracy'],
    topk=(1, 5))
log_config = dict(
    interval=20, hooks=[
        dict(type='TextLoggerHook'),
    ])
dist_params = dict(backend='nccl')
log_level = 'INFO'
work_dir = './work_dirs/' + config_id
load_from = None
resume_from = work_dir + '/latest.pth'
find_unused_parameters = False

omnisource = False
seed = 0
gpu_ids=range(4)
