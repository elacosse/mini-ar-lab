# Pull docker image from https://github.com/ml-tooling/ml-workspace

docker run -d \
    -v "/${PWD}:/workspace" \
    -p 8080:8080 \
    -p 8501:8501 \
    --name "mini-ar-lab" \
    --env AUTHENTICATE_VIA_JUPYTER="mini-ar-lab" \
    --ipc=host \
    --gpus all \
    --shm-size 512m \
    --restart always \
    ml-workspace-gpu:latest

