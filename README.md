MINI-AR-LAB
==============================

ML Pipeline for building Action Recognition (AR) models

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── interim        <- Intermediate data that has been transformed.
    |   ├── examples       <- Some sample model input/outputs
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │   ├── checkpoints    <- Saved checkpoints for later deployment.
    │   ├── configs        <- Configuration files for models employed.
    |   │     ├── mmaction <- Action recognition models
    |   |     ├── mmdet    <- Object/Person detection models
    |   |     └── mmpose   <- Pose estimation models
    │   └── outputs        <- Model outputs from testing
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to generate data for training
    |   |   ├── make_dataset_{DATA_ID}.py                    <- Creates pose skeletal data in DATA_ROOT/DATA_ID/processed.
    |   |   ├── make_annotations.py                          <- Creates annotation files, e.g., RGB/skeletal for model training.
    |   |   ├── make_dataset_static.py                       <- Creates static pose model annotations.   
    │   │   └── utils.py                                     <- Helper functions for data processing specific things.
    │   │
    │   ├── features       <- Scripts generate any relevant model features, e.g., skeletal information
    |   |   ├── Pose.py                 <- Pose model object for handling skeleton computation
    |   |   ├── skeleton_pipelines.py   <- Custom mmaction specific data processing pipeline 
    │   │   └── utils.py                <- Helper functions for feature handling specific things.
    │   │
    │   ├── live           <- Streamlit frontend for interfaces to real-time implementation or inspecting model outputs
    │   │   │            
    |   |   ├── sl_frontend.py          <- Loading live models and using camera feed over web interface.
    |   |   ├── sl_inspector.py         <- Inspector utility for model outputs.
    │   │   └── utils.py                <- Helper functions for live handling specific things.
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    |   |   ├── Detector.py             <- Top-down Detector object for action recognition detection start.
    |   |   ├── Model.py                <- Action-Recognition Model object
    |   |   ├── train.py                <- Scripts to train mmaction models from configuration file
    |   |   ├── test.py                 <- Scripts to test mmaction models from configuration file
    |   |   ├── dist_{train/test}.sh    <- Bash script to handle mmaction distributed GPU training/testing
    │   │   └── utils.py                <- Helper functions for model handling specific things.
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io

***
## Environment Setup

* Use a docker container tested to work, e.g., ml-workspace.
For more detailed instructions on how to get Docker working with your system's GPU hardware,
please refer to https://github.com/ml-tooling/ml-workspace and associated docs.
* To get container up, use ``bash docker.sh''. Note the password credentials for login.

* Build the conda environment mini-ar-lab,
``conda env create -f environment.yml''
* Ensure you have a .env file pointing to data directory where data should be, e.g.,
    DATA_ROOT=/workspace/git/mini-ar-lab/data
* To activate environment, run ``conda activate mini-ar-lab''
* Install the latest MMCV dependencies by running  ``python mim_install.py''

How to train a model and use for live deployment

This example utilizes the full 249 class IsoGD dataset 
to use a skeletal and RGB model.

### 1. Download and gather data into appropriate folder.

All raw data dumps from original sources should be included in `data/raw/`.
For the following example, separate train/valid/test labels are included
as CSVs.

### 2. Process videos from dataset to make skeletons : `make data-IsoGD`

Creates output directory for each video with bounding boxes, keypoints and
resulting video output, see e.g., 
`data/processed/IsoGD/IsoGD_phase_1/train/002/M_00201`
This process takes many hours and must be completed for full annotation file output.
Different pose estimation models are availables in `models/configs/mmpose`.
All examples rely on a SOTA whole-body COCO model, e.g., 
`wbcoco_mmpose_config.yml`

Annotation files holding the skeletal data are created from these 
pose model outputs, or in the case of RGB, a text annotation of what 
raw videos are to feed for later dataloader creation.

Annotation files are located within `data/processed/IsoGD/annotations_11`
where the last number appended is the size of the action class.
Skeletal annotations files are `pkl` files holding a list of dictionaries
with keypoints among other data attributes.


### 3. Training models from generated annotation files : `make train-posec`

Training is handled via a high-level pytorch API called mmaction2. 
Configuration files for model training are located in 
`models/configs/mmaction`. 
Training model output is generated in `work_dir/` where model
checkpoints are saved according to the configuration file. 
All training can be done in a distributed environment.

### 4. Testing model results : `make test-models`

Generating model outputs on a test-set is performed on annotation files labeled
test.
Model outputs are stored in `models/outputs` to be inspected and summarized 
in an evaluation summary, e.g., the streamlit interactive tool.

### 5. Evaluating model results with interative tool : `live/sl_inspector.py`
![](data-inspector.gif)
This data utility provides an overview of model results, summarizing metrics, and investigating data quality issues, e.g., pose estimation results.

### 6. Deploying trained model in real-time system : `live/sl_frontend.py`
A top-down action recognizer implemented to work via Streamlit-WebRTC.

***
## Data Sources

**IsoGD** : [ChaLearn continuous gesture set](https://gesture.chalearn.org/2016-looking-at-people-cvpr-challenge/isogd-and-congd-datasets)

**ConGD** : [ChaLearn continuous gesture set](https://gesture.chalearn.org/2016-looking-at-people-cvpr-challenge/isogd-and-congd-datasets)

**SLR** : [ChaLearn sign language recognition set](https://openaccess.thecvf.com/content/CVPR2021W/ChaLearn/papers/Sincan_ChaLearn_LAP_Large_Scale_Signer_Independent_Isolated_Sign_Language_Recognition_CVPRW_2021_paper.pdf)

***
## Data Preparation

Available data-ids: IsoGD, ConGD, SLR
Annotation files (.pkl for skeletal or .txt for RGB) must be created to train models.  

To generate skeletal files, pose estimation must be run over videos first.
```bash
python src/data/make_dataset_IsoGD.py train models/configs/mmpose/wbcoco_mmpose_config.yml 1 data/processed/IsoGD
```


Annotation file for dataloaders are created from these pose estimation outputs via `make_annotations.py`
```bash
python src/data/make_annotations.py IsoGD train skeleton wbcoco_mmpose_config data/processed/IsoGD 
```
***
## Skeleton Pose Generation (features)

Generating human skeletons via pose estimation are handled through the Pose class.

***
## Model Training and Testing (models)
Available model-ids: mlp, i3d, posec 

`make train-{model-id}`

***
## Model Output and Performance Investigation
Generating model output on test data.

`make test-models`

***
## Real-time Deployment (live)
Real-time action recognition involves both an "isolated" action recognizer and identifying when the action starts and stops.
Separate models are employed to identify start/stop periods and classifying the action.

Different pose and action models can be selected from the drop-down menus.

```bash
python src/live/sl_frontend.py
```
***



