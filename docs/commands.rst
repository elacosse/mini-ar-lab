Commands
========

The Makefile contains the central entry points for common tasks related to this project.

* `make create_environment` will create the appropriate conda environment, e.g., `mini-ar-lab` from `environment.yml`.

Running pose estimation on datasets and creating their annotations
^^^^^^^^^^^^^^^^^^

* `make data-{DATA_ID}` will run pose estimation on data DATA_ID and create their annotations by calling `make_dataset_{DATA_ID}.py`.

* `make data-static-pose` will retrieve poses at particular frames, e.g., mid-point in isolated sets.

Training models
^^^^^^^^^^^^^^^^^^
* `make train-{MODEL_ID}` will train MODEL across different datasets on distributed GPU hardware.


Testing models
^^^^^^^^^^^^^^^^^^
* `make test-model` will test models according to their configuration ID specified in `CONFIG_ID`.

Inspecting models
^^^^^^^^^^^^^^^^^^
* `streamlit run src/live/sl_inspector.py` will run a web utility for inspecting and comparing model performance across different models and their data.


Syncing data to S3
^^^^^^^^^^^^^^^^^^

* `make sync_data_to_s3` will use `aws s3 sync` to recursively sync files in `data/` up to `s3://[OPTIONAL] your-bucket-for-syncing-data (do not include 's3://')/data/`.
* `make sync_data_from_s3` will use `aws s3 sync` to recursively sync files from `s3://[OPTIONAL] your-bucket-for-syncing-data (do not include 's3://')/data/` to `data/`.
