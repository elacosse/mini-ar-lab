from mim import install

install('mmcv-full', find_url='https://github.com/open-mmlab/mmcv.git')
install('mmdet', find_url='https://github.com/open-mmlab/mmdetection.git')
install('mmtrack', find_url='https://github.com/open-mmlab/mmtracking.git')
install('mmpose', find_url='https://github.com/open-mmlab/mmpose.git')
install('mmaction2', find_url='https://github.com/open-mmlab/mmaction2.git')
