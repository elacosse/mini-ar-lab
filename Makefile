.PHONY: clean data lint requirements sync_data_to_s3 sync_data_from_s3

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
BUCKET = [OPTIONAL] your-bucket-for-syncing-data (do not include 's3://')
PROFILE = default
PROJECT_NAME = mini-ar-lab
PYTHON_INTERPRETER = python3
#################################################################################
# Include environment variables with data root and python path
include .env
export
#################################################################################
ifeq (,$(shell which conda))
HAS_CONDA=False
else
HAS_CONDA=True
endif

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Install Python Dependencies
requirements: test_environment
#	 $(PYTHON_INTERPRETER) -m pip install -U pip setuptools wheel
#	 $(PYTHON_INTERPRETER) -m pip install -r requirements.txt

data-pose-SLR:
	$(PYTHON_INTERPRETER) src/data/make_dataset_SLR.py train wbcoco_mmpose_config.yml 1
	$(PYTHON_INTERPRETER) src/data/make_annotations.py SLR train skeleton wbcoco_mmpose_config 

	$(PYTHON_INTERPRETER) src/data/make_dataset_SLR.py valid wbcoco_mmpose_config.yml 1
	$(PYTHON_INTERPRETER) src/data/make_annotations.py SLR valid skeleton wbcoco_mmpose_config

	$(PYTHON_INTERPRETER) src/data/make_dataset_SLR.py test wbcoco_mmpose_config.yml 1
	$(PYTHON_INTERPRETER) src/data/make_annotations.py SLR test skeleton wbcoco_mmpose_config
data-rgb-SLR:
	$(PYTHON_INTERPRETER) src/data/make_annotations.py SLR train rgb wbcoco_mmpose_config
	$(PYTHON_INTERPRETER) src/data/make_annotations.py SLR valid rgb wbcoco_mmpose_config
	$(PYTHON_INTERPRETER) src/data/make_annotations.py SLR test rgb wbcoco_mmpose_config

data-pose-IsoGD:
	$(PYTHON_INTERPRETER) src/data/make_dataset_IsoGD.py train wbcoco_mmpose_config.yml 1
	$(PYTHON_INTERPRETER) src/data/make_annotations.py IsoGD train skeleton wbcoco_mmpose_config 
	$(PYTHON_INTERPRETER) src/data/make_annotations.py IsoGD train skeleton wbcoco_mmpose_config selected_labels.json
	
	$(PYTHON_INTERPRETER) src/data/make_dataset_IsoGD.py valid wbcoco_mmpose_config.yml 1
	$(PYTHON_INTERPRETER) src/data/make_annotations.py IsoGD valid skeleton wbcoco_mmpose_config
	$(PYTHON_INTERPRETER) src/data/make_annotations.py IsoGD valid skeleton wbcoco_mmpose_config selected_labels.json
	
	$(PYTHON_INTERPRETER) src/data/make_dataset_IsoGD.py test wbcoco_mmpose_config.yml 1
	$(PYTHON_INTERPRETER) src/data/make_annotations.py IsoGD test skeleton wbcoco_mmpose_config 
	$(PYTHON_INTERPRETER) src/data/make_annotations.py IsoGD test skeleton wbcoco_mmpose_config selected_labels.json 
data-rgb-IsoGD:
	$(PYTHON_INTERPRETER) src/data/make_annotations.py IsoGD train rgb wbcoco_mmpose_config 
	$(PYTHON_INTERPRETER) src/data/make_annotations.py IsoGD train rgb wbcoco_mmpose_config selected_labels.json
	
	$(PYTHON_INTERPRETER) src/data/make_annotations.py IsoGD valid rgb wbcoco_mmpose_config  
	$(PYTHON_INTERPRETER) src/data/make_annotations.py IsoGD valid rgb wbcoco_mmpose_config selected_labels.json
	
	$(PYTHON_INTERPRETER) src/data/make_annotations.py IsoGD test rgb wbcoco_mmpose_config 
	$(PYTHON_INTERPRETER) src/data/make_annotations.py IsoGD test rgb wbcoco_mmpose_config selected_labels.json

data-pose-ConGD:
	$(PYTHON_INTERPRETER) src/data/make_dataset_ConGD.py train wbcoco_mmpose_config.yml 1
	$(PYTHON_INTERPRETER) src/data/make_annotations.py ConGD train skeleton wbcoco_mmpose_config
	$(PYTHON_INTERPRETER) src/data/make_annotations.py ConGD train skeleton wbcoco_mmpose_config selected_labels.json

	$(PYTHON_INTERPRETER) src/data/make_dataset_ConGD.py valid wbcoco_mmpose_config.yml 1
	$(PYTHON_INTERPRETER) src/data/make_annotations.py ConGD valid skeleton wbcoco_mmpose_config 
	$(PYTHON_INTERPRETER) src/data/make_annotations.py ConGD valid skeleton wbcoco_mmpose_config selected_labels.json

	$(PYTHON_INTERPRETER) src/data/make_dataset_ConGD.py test wbcoco_mmpose_config.yml 1
	$(PYTHON_INTERPRETER) src/data/make_annotations.py ConGD test skeleton wbcoco_mmpose_config  
	$(PYTHON_INTERPRETER) src/data/make_annotations.py ConGD test skeleton wbcoco_mmpose_config selected_labels.json
data-rgb-ConGD:
	$(PYTHON_INTERPRETER) src/data/make_annotations.py ConGD train rgb wbcoco_mmpose_config
	$(PYTHON_INTERPRETER) src/data/make_annotations.py ConGD train rgb wbcoco_mmpose_config selected_labels.json

	$(PYTHON_INTERPRETER) src/data/make_annotations.py ConGD valid rgb wbcoco_mmpose_config
	$(PYTHON_INTERPRETER) src/data/make_annotations.py ConGD valid rgb wbcoco_mmpose_config selected_labels.json

	$(PYTHON_INTERPRETER) src/data/make_annotations.py ConGD test rgb wbcoco_mmpose_config
	$(PYTHON_INTERPRETER) src/data/make_annotations.py ConGD test rgb wbcoco_mmpose_config selected_labels.json

data-IsoGD-lookup: requirements
#	$(PYTHON_INTERPRETER) src/data/make_dataset_precom_IsoGD_staticNNLookUp.py train models/wbcoco_mmpose_config.yml data/processed/IsoGDLookup
#   $(PYTHON_INTERPRETER) src/data/make_dataset_IsoGD_staticNNLookUp.py train models/wbcoco_mmpose_config.yml data/processed/IsoGDLookup
	$(PYTHON_INTERPRETER) src/data/make_dataset_IsoGD_staticNNLookUp.py valid models/wbcoco_mmpose_config.yml data/processed/IsoGDLookup
	$(PYTHON_INTERPRETER) src/data/make_dataset_IsoGD_staticNNLookUp.py test models/wbcoco_mmpose_config.yml data/processed/IsoGDLookup

data-UTD-MHAD: requirements
	$(PYTHON_INTERPRETER) src/data/make_dataset_UTD-MHAD.py data/raw/UTD-MHAD data/processed/UTD-MHAD

data-static-pose:
	# SLR
	$(PYTHON_INTERPRETER) src/data/make_dataset_static.py train SLR 226 0.0
	$(PYTHON_INTERPRETER) src/data/make_dataset_static.py valid SLR 226 0.0
	$(PYTHON_INTERPRETER) src/data/make_dataset_static.py test SLR 226 0.0
	# IsoGD - 249 classes
	$(PYTHON_INTERPRETER) src/data/make_dataset_static.py train IsoGD 249 0.0
	$(PYTHON_INTERPRETER) src/data/make_dataset_static.py valid IsoGD 249 0.0
	$(PYTHON_INTERPRETER) src/data/make_dataset_static.py test IsoGD 249 0.0
	# IsoGD - 11 classes
	$(PYTHON_INTERPRETER) src/data/make_dataset_static.py train IsoGD 11 0.0
	$(PYTHON_INTERPRETER) src/data/make_dataset_static.py valid IsoGD 11 0.0
	$(PYTHON_INTERPRETER) src/data/make_dataset_static.py test IsoGD 11 0.0

# data-detector:

## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete

## Lint using flake8
lint:
	flake8 src

## Upload Data to S3
sync_data_to_s3:
ifeq (default,$(PROFILE))
	aws s3 sync data/ s3://$(BUCKET)/data/
else
	aws s3 sync data/ s3://$(BUCKET)/data/ --profile $(PROFILE)
endif

## Download Data from S3
sync_data_from_s3:
ifeq (default,$(PROFILE))
	aws s3 sync s3://$(BUCKET)/data/ data/
else
	aws s3 sync s3://$(BUCKET)/data/ data/ --profile $(PROFILE)
endif


## Test python environment is setup correctly
test_environment:
	$(PYTHON_INTERPRETER) test_environment.py

## Set up python interpreter environment
# conda config --add create_default_packages PACKAGENAME1 PACKAGENAME2
create_environment:
ifeq (True,$(HAS_CONDA))
		@echo ">>> Detected conda, creating conda environment."
ifeq (3,$(findstring 3,$(PYTHON_INTERPRETER)))
	conda env create --name $(PROJECT_NAME) -f environment.yml
endif
	@echo ">>> New conda env created. Activate with:\nsource activate $(PROJECT_NAME)"
else
	@echo ">>> create_environment failed! Check conda. \n"
endif

update_environment:
ifeq (True,$(HAS_CONDA))
		@echo ">>> Detected conda, updating conda environment."
ifeq (3,$(findstring 3,$(PYTHON_INTERPRETER)))
	conda env update --name $(PROJECT_NAME) --file environment.yml --prune

endif
	@echo ">>> Conda env updated."
else
	@echo ">>> update_environment failed! Check conda. \n"
endif


# ## Set up python interpreter environment
# create_environment:
# ifeq (True,$(HAS_CONDA))
# 		@echo ">>> Detected conda, creating conda environment."
# ifeq (3,$(findstring 3,$(PYTHON_INTERPRETER)))
# 	conda create --name $(PROJECT_NAME) python=3
# else
# 	conda create --name $(PROJECT_NAME) python=2.7
# endif
# 		@echo ">>> New conda env created. Activate with:\nsource activate $(PROJECT_NAME)"
# else
# 	$(PYTHON_INTERPRETER) -m pip install -q virtualenv virtualenvwrapper
# 	@echo ">>> Installing virtualenvwrapper if not already installed.\nMake sure the following lines are in shell startup file\n\
# 	export WORKON_HOME=$$HOME/.virtualenvs\nexport PROJECT_HOME=$$HOME/Devel\nsource /usr/local/bin/virtualenvwrapper.sh\n"
# 	@bash -c "source `which virtualenvwrapper.sh`;mkvirtualenv $(PROJECT_NAME) --python=$(PYTHON_INTERPRETER)"
# 	@echo ">>> New virtualenv created. Activate with:\nworkon $(PROJECT_NAME)"
# endif



#################################################################################
# PROJECT RULES                                                                 #
#################################################################################
CUDA_VISIBLE_DEVICES=0,1,2,3

# Baselines
train-mlp:
	$(PYTHON_INTERPRETER) src/models/train_mlp_baseline.py \
		models/configs/mmaction/mlp_pose_1x1x1_150e_data-SLR.py
	$(PYTHON_INTERPRETER) src/models/train_mlp_baseline.py \
		models/configs/mmaction/mlp_pose_1x1x1_150e_data-IsoGD_249.py
	$(PYTHON_INTERPRETER) src/models/train_mlp_baseline.py \
		models/configs/mmaction/mlp_pose_1x1x1_150e_data-IsoGD_11.py

train-mlp-detector:
	$(PYTHON_INTERPRETER) src/models/train_mlp_detector.py \
		models/configs/mmaction/mlp_pose_8x2x1_150e_data-detector.py
# RGBs
train-i3d:
	# $(PYTHON_INTERPRETER) ../mmaction2/tools/train.py \
		# 	models/configs/mmaction/i3d_r50_video_32x2x1_100e_SLR_rgb_data-IsoGD.py \
		# 	--cfg-options model.cls_head.num_classes=249
	# CUDA_VISIBLE_DEVICES=-0,1,2,3 bash ../mmaction2/tools/dist_train.sh \
	# 	models/configs/mmaction/i3d_r50_video_32x2x1_100e_kinetics400_rgb_data-SLR.py \
	# 	4
	CUDA_VISIBLE_DEVICES=-0,1,2,3 bash ../mmaction2/tools/dist_train.sh \
		models/configs/mmaction/i3d_r50_video_32x2x1_100e_SLR_rgb_data-IsoGD_249.py \
		4
	# CUDA_VISIBLE_DEVICES=-0,1,2,3 bash ../mmaction2/tools/dist_train.sh \
	# 	models/configs/mmaction/i3d_r50_video_32x2x1_100e_IsoGD249_rgb_data-IsoGD_11.py \
	# 	4 \

# GCNs
train-posec:
	# CUDA_VISIBLE_DEVICES=1,2,3 $(PYTHON_INTERPRETER) src/models/train.py \
	# 	models/configs/mmaction/posec_32x2x1_ntu120_data-SLR.py \

	CUDA_VISIBLE_DEVICES=0,1,2,3 bash src/models/dist_train.sh \
	models/configs/mmaction/posec_16x4x1_IsoGD249_data-IsoGD_11.py  \
	4

	# CUDA_VISIBLE_DEVICES=0,1,2,3 bash src/models/dist_train.sh \
	# 	models/configs/mmaction/posec_32x2x1_ntu120_data-SLR.py  \
	# 	4
	# CUDA_VISIBLE_DEVICES=1,2,3 bash src/models/dist_train.sh \
	# 	models/configs/mmaction/posec_32x2x1_SLR_data-IsoGD_249.py  \
	# 	3
	# CUDA_VISIBLE_DEVICES=0,1,2,3 bash src/models/dist_train.sh \
	# 	models/configs/mmaction/posec_32x2x1_IsoGD249_data-IsoGD_11.py  \
	# 	4

# Testing Suite
# CONFIG_ID=i3d_r50_video_32x2x1_100e_kinetics400_rgb_data-SLR
# CONFIG_ID=i3d_r50_video_32x2x1_100e_SLR_rgb_data-IsoGD_249
# CONFIG_ID=i3d_r50_video_32x2x1_100e_IsoGD249_rgb_data-IsoGD_11

# CONFIG_ID=posec_32x2x1_ntu120_data-SLR
# CONFIG_ID=posec_32x2x1_SLR_data-IsoGD_249
CONFIG_ID=posec_16x4x1_IsoGD249_data-IsoGD_11.py
# CONFIG_ID=posec_32x2x1_IsoGD249_data-IsoGD_11

test-models:
	CUDA_VISIBLE_DEVICES=0,1,2,3 bash src/models/dist_test.sh \
		models/configs/mmaction/${CONFIG_ID}.py \
		work_dirs/${CONFIG_ID}/latest.pth 4 \
		--out models/outputs/${CONFIG_ID}_cv-test.pkl \ 

	# $(PYTHON_INTERPRETER) src/models/test.py \
	# 	models/configs/mmaction/${CONFIG_ID}.py \
	# 	work_dirs/${CONFIG_ID}/latest.pth \
	# 	--cfg-options workers_per_gpu=4 videos_per_gpu=4 \
	# 	--out models/outputs/${CONFIG_ID}_cv-test.pkl \

test-mlp:
	# MLP
	$(PYTHON_INTERPRETER) src/models/test_mlp_baseline.py \
		models/configs/mmaction/mlp_pose_1x1x1_150e_data-SLR.py
	$(PYTHON_INTERPRETER) src/models/test_mlp_baseline.py \
		models/configs/mmaction/mlp_pose_1x1x1_150e_data-IsoGD_249.py
	$(PYTHON_INTERPRETER) src/models/test_mlp_baseline.py \
		models/configs/mmaction/mlp_pose_1x1x1_150e_data-IsoGD_11.py

train-isogd-rgb: requirements
#	$(PYTHON_INTERPRETER) src/models/train_rgb_isogd.py tsn 1 1 8 64 1
	$(PYTHON_INTERPRETER) src/models/train_rgb_isogd.py slowonly 8 8 1 64 1

# test-detector:
# 	$(PYTHON_INTERPRETER) src/models/test_detector.py 


MODEL_CONFIG_PATH=models/configs/mmaction/continuous/posec_16x1x1_IsoGD11_data.py
DETECTOR_CONFIG_PATH=models/configs/mmaction/mlp_pose_8x2x1_150e_data-detector.py
test-continuous:
	$(PYTHON_INTERPRETER) src/models/test_continuous.py $(MODEL_CONFIG_PATH) $(DETECTOR_CONFIG_PATH)

#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
